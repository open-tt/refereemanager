# Referee Manager

The referee manager is a free, open tool for managing table tennis referees and their assignments.

Part of the project "Open-TT" which provides open documents and applications for table tennis:

- <https://gitlab.com/open-tt>

At the moment, there is no stable, productive version available.
Several features are working, see [changelog](changelog.md).

**Important:** the focus of development at the moment is *features*, not safety.
Backup your files regularly, please.

## Downloads

All downloads are provided via sourceforge:

- latest version → <https://sourceforge.net/projects/refereemanager/files/latest/download>
- all downloads → <https://sourceforge.net/projects/refereemanager/files/>
- sourceforge project site → <https://sourceforge.net/projects/refereemanager/>

## Manuals

There are two manuals, one for developers, one for users, both are provided via Read the Docs:

- latest version (HTML) → <https://refereemanager.readthedocs.io/>
- latest version (PDF) → <https://readthedocs.org/projects/refereemanager/downloads/pdf/latest/>
- latest version (Epub) → <https://readthedocs.org/projects/refereemanager/downloads/epub/latest/>
- all versions → <https://readthedocs.org/projects/refereemanager/versions/>
- Read the Docs project site → <https://readthedocs.org/projects/refereemanager/>

## Templates

Communication with referees is done using templates, you find examples and basic templates in the Referee Manager Templates repository:

- <https://gitlab.com/open-tt/refereemanager-templates>

## Git-Repository

The branching model regards to the stable mainline model described in <http://www.bitsnbites.eu/a-stable-mainline-branching-model-for-git>

This means, there is always a stable mainline, the `master` branch.
This branch ist always compileable and testable, both without errors.

For more details, see [developer documentation](https://refereemanager.readthedocs.io/de/latest/developer/structure.html).

## Legal stuff

### Licenses

License of the documents: Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License
See file [LICENSE](LICENSE).

License of the programs: GNU General Public License.
See file [COPYING](COPYING).

Which means:

- the documents are free, as long as you
	- don't make money with them
	- mention the creator
	- share derivates with the same license
- programs are free and open source
	- you can use the program as you want, even commercially
	- you can share the program as you like
	- if you change the source code, you have to distribute it under the same license, and you have to provide the source code of the programs


### Copyright

Copyright 2016-2020 Ekkart Kleinod <ekleinod@edgesoft.de>

The program is distributed under the terms of the GNU General Public License.

See [COPYING](COPYING) for details.

This file is part of Open-TT: Referee Manager.

Open-TT: Referee Manager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Open-TT: Referee Manager is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
