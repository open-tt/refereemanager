# refereemanager install script.

## Legal stuff
#
# Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
#
# This file is part of Open-TT: Referee Manager.
#
# Open-TT: Referee Manager is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Open-TT: Referee Manager is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
#
# @author Ekkart Kleinod
# @since 0.14.0

# use encoding: ISO-8859-15

!define PROJECT_NAME Refereemanager
!define VERSION **version**
!define LONG_VERSION "**longversion**"
!define FILE_VERSION "**fileversion**"
!define COMPANY "Ekkart Kleinod (edge-soft)"
!define URL http://www.edgesoft.de/
!define LONGNAME "refman - Der Referee-Manager"
!define FILENAME "refereemanager"

!define DIRNAME ${FILENAME}

!define DIR_RESOURCES "..\..\refereemanager\src\main\resources\de\edgesoft\refereemanager\resources\"
!define DIR_FILES "..\..\files\"

!include ..\..\submodules\edgeutils\nsis\simple-jar.nsi

# EOF
