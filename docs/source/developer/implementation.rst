Implementation details
======================

.. index:: implementation

Here you find some details/tips about/for the implementation.

Create a new Overview
---------------------

.. index:: overview view

An overview contains a table on the left, a detail view on the right, and below the detail view the buttons for editing the data.

An example is the :guilabel:`referee overview`, visible using menu :menuselection:`Menschen --> Schiriübersicht`, or :kbd:`Control-Alt-R`.

For a new overview you have to adapt/create at least the following files (example overview for *trainees*, all files relative to the :file:`de.edgesoft.refereemanager` package):

.. rubric:: Adapt

- :file:`view.AppLayout.fxml`
- :file:`controller.AppLayoutController.java`

.. rubric:: Overview and Details views

- :file:`view.datatables.DataTableTrainees.fxml`
- :file:`controller.datatables.DataTableTraineesController.java`
- :file:`view.details.DetailsTrainee.fxml`
- :file:`controller.details.DetailsTraineeController.java`
- :file:`controller.overview.OverviewTraineesController.java`

.. rubric:: Editing dialog

- :file:`view.editdialogs.EditDialogTrainee.fxml`
- :file:`controller.editdialogs.EditDialogTraineeController.java`
- :file:`view.inputforms.InputForm{xyz}.fxml` if needed
- :file:`controller.inputforms.InputForm{xyz}Controller.java` if needed
