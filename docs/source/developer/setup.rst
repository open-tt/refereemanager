Setup your working environment
==============================

.. index:: setup

.. highlights::
  Get working...

.. toctree::
  :maxdepth: 1
  :caption: See also:

  structure
  software

At the moment, the :program:`edgeutils` project has to be installed locally via :samp:`maven install`.
This will be fixed in the future.

In short, do the following:

#. check out the :program:`refereemanager` and the :program:`edgeutils` repository
#. update/check out the submodules
#. import the :program:`eclipse` projects :file:`de.edgesoft.edgeutils` and :file:`edgeutils-modules`
#. wait for the :program:`maven` dependencies to download and install
#. install :program:`edgeutils` via :samp:`maven install` on :file:`edgeutils-modules`
#. import the :program:`eclipse` project :file:`refereemanager`
#. wait for the :program:`maven` dependencies to download and install
#. check, if the :program:`refereemanager` can be compiled and executed
#. start development

refereemanager repository
  https://gitlab.com/open-tt/refereemanager/

edgeutils repository
  https://gitlab.com/ekleinod/edgeutils/
