Structure, Code, and Files
==========================

.. index:: structure, code, files

.. highlights::
  Links and workflows.

Structure
---------

The repository is structured as follows:

.. digraph:: structure
  :caption: Repository Structure.

  graph [splines="line"];
  node [shape="box", style="filled", fillcolor="2", colorscheme="blues5"];
  refereemanager [fillcolor="1"];
  refereemanager2 [label = "refereemanager"];
  refereemanager -> build;
  refereemanager -> docs;
  refereemanager -> documentation;
  refereemanager -> refereemanager2;
  refereemanager -> submodules;

:file:`build`
  build files for releases
:file:`documentation`
  documentation, especially user, developer, and API documentation
:file:`files`
  released files
:file:`refereemanager`
  eclipse project with sources, ressources and tests
:file:`submodules`
  needed submodules (edgeutils for Ant and JAXB commons`

Git-Repository
--------------

The :program:`git` repository is maintained using :program:`gitlab`.
:program:`gitlab` is used for code maintenance and issue tracking.

Repository
  https://gitlab.com/open-tt/refereemanager/
Issue Tracker
  https://gitlab.com/open-tt/refereemanager/issues

The branching model regards to the stable mainline model described in http://www.bitsnbites.eu/a-stable-mainline-branching-model-for-git/.

This means, there is always a stable mainline, the :file:`master` branch.
This branch ist always compileable and testable, both without errors.

Features are developed using :file:`feature` branches.
Special feature branches are used (different from the mainline model) for finalizing releases.

Releases are created as branches of the mainline.
Additionally, each release is tagged, tags contain the patch and, if needed, additional identifiers, such as :file:`rc1` or :file:`beta`.

Patches are made in the according release branch.
Minor version changes get their own release branch.


.. rubric:: Naming Feature Branches (with ticket)

- :file:`feature/t{ticket number}-{description}`
- :file:`feature/t43-stable_mainline`
- :file:`feature/t39-use_kotlin`

.. rubric:: Naming Feature Branches (without ticket)

- :file:`feature/{description}`
- :file:`feature/documentation`

.. rubric:: Naming Feature Branches for Finalizing Releases

- :file:`feature/release-{major}.{minor}.{patch}[-{additional}]`
- :file:`feature/release-0.15.0`
- :file:`feature/release-0.17.0-pre1`

.. rubric:: Naming Release Branches

- :file:`release/{major}.{minor}`
- :file:`release/0.15`
- :file:`release/1.0`

.. rubric:: Naming Tags

- :file:`{major}.{minor}.{patch}[-{additional}]`
- :file:`0.15.0`
- :file:`1.0.0-beta`
- :file:`1.0.0-rc1`
- :file:`1.0.0`

Releases
--------

See also: :doc:`release`

Releases are tagged in the :program:`git` repository:

- https://gitlab.com/open-tt/refereemanager/tags

The according binaries are maintained at :program:`sourceforge`:

- https://sourceforge.net/projects/refereemanager/
- https://sourceforge.net/projects/refereemanager/files/
