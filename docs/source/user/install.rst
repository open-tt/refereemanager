Installation
============

Derzeit mit der Holzhammermethode:

- Java 12 des OpenJDK installieren
- neueste jar-Datei des *RefereeManager* herunterladen
- jar-Datei mit Java aufrufen :samp:`java -jar refereemanager.jar` bzw. :samp:`java -jar refereemanager-pre.jar`

Downloads:

- `OpenJDK <https://jdk.java.net/>`_
- `RefereeManager <https://sourceforge.net/projects/refereemanager/files/latest/download>`_

.. todo:: Installationsanleitung verbessern
