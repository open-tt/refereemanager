Einleitung
==========

.. highlights::
	Was soll das und wie kann ich mitmachen?

Der *RefereeManager* ist eine Schiedsrichterverwaltung für Tischtennisschiedsrichterinnen.

Dass andere Sportarten nicht explizit unterstützt werden, schließt nicht aus, dass der *RefereeManager* in Teilbereichen nicht doch nützlich sein kann.
Wenn man die angebotenen Features nutzen möchte – voilà.

Der RefereeManager ist ein kostenloses, freies und quelloffenes (Open Source) Programm.
Das wird auch so bleiben, eine Kommerzialisierung ist nicht geplant und wird nicht stattfinden.
Die Quellen liegen unter https://gitlab.com/open-tt/refereemanager

Features
--------

Folgende Features bietet der RefereeManager (bzw. wird bieten):

.. rubric:: 1.0.0 – Meilenstein `Basics <https://gitlab.com/open-tt/refereemanager/-/milestones/1>`_

- Verwaltung von Personen (Schiedsrichterinnen, Vereinsverantwortliche, Berichtsempfängerinnen, Auszubildende, ...) und deren Daten
	- Name und Titel
	- Adressen
	- Aus- und Fortbildung
	- Status
	- Geburtstage, Todestage
- Verwaltung von Clubs
- Verwaltung von Ligen
- Verwaltung von zusätzlich notwendigen Daten (Geschlechter, Status, Ausbildungsarten, ...)
- Kommunikation per E-Mail mit Personen
- Erzeugung von Dokumenten oder Listen aus den Daten

.. rubric:: 2.0.0 – Meilenstein `Dates, Assignments, Teams <https://gitlab.com/open-tt/refereemanager/-/milestones/2>`_

- Verwaltung von Terminen
- Verwaltung von Schiedsrichtereinsätzen
- Verwaltung von Turniereinsätzen
- Verwaltung von Teams

.. rubric:: 3.0.0 – Meilenstein `Refinement <https://gitlab.com/open-tt/refereemanager/-/milestones/3>`_

- andere Sprachen
- Import von Daten anderer Systeme
- Bereinigung der Datenbasis
- Behandlung der Daten ausgeschiedener Personen

Daten
-----

Die Daten des *RefereeManager* werden in XML-Dateien lokal auf dem Rechner abgelegt.
Pro Saison wird eine XML-Datei erstellt.

Das heißt, für die Nutzung muss man nicht online sein und für die Datensicherung ist jede Nutzerin selbst verantwortlich.
Diese Entscheidung wurde bewusst getroffen, es ist auch nicht geplant, eine Onlinespeicherung zu implementieren.

Das heißt ebenfalls, dass die meisten Daten der Personen redundant pro Saison gespeichert werden.
Diese Entscheidung wurde ebenfalls bewusst getroffen, da sonst die Datenmodellierung insbesondere des Archivs deutlich komplizierter geworden wäre.
So müssen Daten kaum historisiert werden.

Mitmachen
---------

Wie erwähnt, ist der *RefereeManager* Open Source, das heißt, jede Nutzerin oder Programmiererin kann sich am Projekt beteiligen oder ihre eigene Version des Programms erstellen.

Das Programm wird von mir (Ekkart Kleinod) in meiner Freizeit entwickelt, das heißt, ich bin frei in der Richtung, die ich dem Programm geben möchte.
Das heißt auch, ich programmiere, wenn ich Zeit und Lust dazu habe.
Interessante Features werde ich umsetzen, wenn sie mir nützen oder eine technische Herausforderung für mich darstellen.

Ergo: wenn etwas schnell umgesetzt werden soll: begeistert mich oder macht das selbst und erstellt einen Pull-Request.
Was nicht hilft (ernsthaft): mir Geld anbieten.

Wenn Ihr Geld ausgeben wollt, sucht Euch eine Programmiererin, die Euer Feature umsetzt und als Pull-Request einreicht.

Wobei könnt Ihr helfen?

- Fehler melden
- Verbesserungen und Wünsche melden
- Übersetzungen in andere Sprachen (ab `Meilenstein 3.0.0 <https://gitlab.com/open-tt/refereemanager/-/milestones/3>`_)
- selbst programmieren

Der Programmcode, Fehler, Wünsche und die Dokumentation werden über :program:`gitlab` gehostet.
Ihr besorgt Euch also am besten einen Account dafür.

Quellcode
	per git in gitlab unter: https://gitlab.com/open-tt/refereemanager

Fehler melden
	in gitlab unter: https://gitlab.com/open-tt/refereemanager/issues

Wünsche melden
	in gitlab unter: https://gitlab.com/open-tt/refereemanager/issues

Übersetzungen
	gebe ich bekannt, wenn es soweit ist, derzeit ist die technische Basis dafür noch nicht da

Siehe auch: :doc:`/developer/index`
