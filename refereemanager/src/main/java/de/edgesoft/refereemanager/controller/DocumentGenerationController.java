package de.edgesoft.refereemanager.controller;

import java.io.File;
import java.io.Writer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import org.apache.logging.log4j.util.Strings;

import de.edgesoft.edgeutils.ThrowableUtils;
import de.edgesoft.edgeutils.commons.Info;
import de.edgesoft.edgeutils.files.FileAccess;
import de.edgesoft.edgeutils.files.Prefs;
import de.edgesoft.edgeutils.files.Resources;
import de.edgesoft.edgeutils.i18n.I18N;
import de.edgesoft.edgeutils.i18n.ResourceType;
import de.edgesoft.edgeutils.javafx.CSSClasses;
import de.edgesoft.edgeutils.javafx.ControlUtils;
import de.edgesoft.edgeutils.javafx.LabeledUtils;
import de.edgesoft.edgeutils.javafx.SceneUtils;
import de.edgesoft.edgeutils.javafx.TableColumnUtils;
import de.edgesoft.edgeutils.javafx.TooltipUtils;
import de.edgesoft.edgeutils.log4j.AppenderUtils;
import de.edgesoft.refereemanager.RefereeManager;
import de.edgesoft.refereemanager.controller.datatables.AbstractDataTableController;
import de.edgesoft.refereemanager.documentgeneration.DataFeature;
import de.edgesoft.refereemanager.documentgeneration.EMailGenerationTask;
import de.edgesoft.refereemanager.documentgeneration.IDocumentGenerationTask;
import de.edgesoft.refereemanager.documentgeneration.LetterGenerationTask;
import de.edgesoft.refereemanager.documentgeneration.MultipleDocumentsGenerationTask;
import de.edgesoft.refereemanager.documentgeneration.MultipleTextsGenerationTask;
import de.edgesoft.refereemanager.documentgeneration.SingleDocumentGenerationTask;
import de.edgesoft.refereemanager.documentgeneration.SingleTextGenerationTask;
import de.edgesoft.refereemanager.documentgenerationqueue.jaxb.DocumentGenerationJob;
import de.edgesoft.refereemanager.documentgenerationqueue.jaxb.DocumentGenerationQueue;
import de.edgesoft.refereemanager.documentgenerationqueue.jaxb.ObjectFactory;
import de.edgesoft.refereemanager.documentgenerationqueue.model.DocumentGenerationJobModel;
import de.edgesoft.refereemanager.messagetext.jaxb.Attachment;
import de.edgesoft.refereemanager.messagetext.jaxb.MessageText;
import de.edgesoft.refereemanager.messagetext.jaxb.MessageTextElement;
import de.edgesoft.refereemanager.messagetext.model.MessageTextModel;
import de.edgesoft.refereemanager.model.TitledIDTypeModel;
import de.edgesoft.refereemanager.utils.AlertUtils;
import de.edgesoft.refereemanager.utils.DocumentGenerationResult;
import de.edgesoft.refereemanager.utils.LocalPrefs;
import de.edgesoft.refereemanager.utils.PrefKey;
import javafx.beans.binding.BooleanExpression;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableBooleanValue;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Controller for the person communication scene.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.10.0
 */
public class DocumentGenerationController {

	/**
	 * Appender name for document generation.
	 *
	 * @since 0.17.0
	 */
	private static final String DOCUMENT_GENERATION_APPENDER = "documentGeneration";


	/**
	 * Split pane.
	 */
	@FXML
	private SplitPane pneSplit;


	/**
	 * Send button.
	 */
	@FXML
	private Button btnStartGeneration;

	/**
	 * Data table selection button.
	 *
	 * @since 0.17.0
	 */
	@FXML
	private Button btnDataTableSelect;

	/**
	 * Prefs button.
	 */
	@FXML
	private Button btnPrefs;

	/**
	 * Message heading.
	 *
	 * @since 0.13.0
	 */
	@FXML
	private Label lblMessage;

	/**
	 * Label title.
	 *
	 * @since 0.12.0
	 */
	@FXML
	private Label lblTitle;

	/**
	 * Textfield title.
	 */
	@FXML
	private TextField txtTitle;

	/**
	 * Label subtitle.
	 *
	 * @since 0.12.0
	 */
	@FXML
	private Label lblSubtitle;

	/**
	 * Textfield subtitle.
	 */
	@FXML
	private TextField txtSubtitle;

	/**
	 * Label opening.
	 *
	 * @since 0.12.0
	 */
	@FXML
	private Label lblOpening;

	/**
	 * Textfield opening.
	 */
	@FXML
	private TextField txtOpening;

	/**
	 * Label body.
	 *
	 * @since 0.13.0
	 */
	@FXML
	private Label lblBody;

	/**
	 * Textarea body.
	 */
	@FXML
	private TextArea txtBody;

	/**
	 * Label closing.
	 *
	 * @since 0.12.0
	 */
	@FXML
	private Label lblClosing;

	/**
	 * Textfield closing.
	 */
	@FXML
	private TextField txtClosing;

	/**
	 * Label signature.
	 *
	 * @since 0.12.0
	 */
	@FXML
	private Label lblSignature;

	/**
	 * Textfield signature.
	 */
	@FXML
	private TextField txtSignature;


	/**
	 * Heading technical.
	 *
	 * @since 0.13.0
	 */
	@FXML
	private Label lblTechnical;

	/**
	 * Label date.
	 *
	 * @since 0.12.0
	 */
	@FXML
	private Label lblDate;

	/**
	 * Textfield date.
	 *
	 * @since 0.12.0
	 */
	@FXML
	private TextField txtDate;

	/**
	 * Label filename.
	 *
	 * @since 0.12.0
	 */
	@FXML
	private Label lblFilename;

	/**
	 * Textfield filename.
	 */
	@FXML
	private TextField txtFilename;


	/**
	 * Label attachments.
	 *
	 * @since 0.12.0
	 */
	@FXML
	private Label lblAttachments;

	/**
	 * Buttonbar attachments.
	 *
	 * @since 0.12.0
	 */
	@FXML
	private ButtonBar barAttachments;

	/**
	 * Attachment add button.
	 */
	@FXML
	private Button btnAttachmentAdd;

	/**
	 * Attachment edit button.
	 */
	@FXML
	private Button btnAttachmentEdit;

	/**
	 * Attachment delete button.
	 */
	@FXML
	private Button btnAttachmentDelete;

	/**
	 * Attachment table view.
	 */
	@FXML
	private TableView<Attachment> tblAttachments;

	/**
	 * Filename column.
	 */
	@FXML
	private TableColumn<Attachment, String> colAttachmentFilename;

	/**
	 * Title column.
	 */
	@FXML
	private TableColumn<Attachment, String> colAttachmentTitle;

	/**
	 * Landscape paper format column.
	 */
	@FXML
	private TableColumn<Attachment, Boolean> colAttachmentLandscape;


	/**
	 * Label communication kind.
	 *
	 * @since 0.13.0
	 */
	@FXML
	private Label lblDocumentGenerationType;

	/**
	 * Box communication kind.
	 *
	 * @since 0.17.0
	 */
	@FXML
	private VBox boxDocumentGenerationType;

	/**
	 * Box for additional document generation task ui elements.
	 *
	 * @since 0.17.0
	 */
	@FXML
	private VBox boxDocumentGenerationTasks;

	/**
	 * Toggle group for document generation types.
	 *
	 * @since 0.17.0
	 */
	private ToggleGroup grpDocumentGenerationType;

	/**
	 * Label message file.
	 *
	 * @since 0.13.0
	 */
	@FXML
	private Label lblMessageFile;

	/**
	 * Label message file name.
	 *
	 * @since 0.13.0
	 */
	@FXML
	private Label lblMessageFilename;

	/**
	 * Textfield message file name.
	 */
	@FXML
	private TextField txtMessageFilename;

	/**
	 * Button message file select.
	 */
	@FXML
	private Button btnMessageFileSelect;

	/**
	 * Button message file load.
	 */
	@FXML
	private Button btnMessageFileLoad;

	/**
	 * Button message file save.
	 */
	@FXML
	private Button btnMessageFileSave;


	/**
	 * Label output path.
	 *
	 * @since 0.12.0
	 */
	@FXML
	private Label lblOutputPath;

	/**
	 * Text field output path.
	 *
	 * @since 0.12.0
	 */
	@FXML
	private TextField txtOutputPath;

	/**
	 * Label options.
	 *
	 * @since 0.12.0
	 */
	@FXML
	private Label lblOptions;

	/**
	 * Text field options.
	 *
	 * @since 0.12.0
	 */
	@FXML
	private TextField txtOptions;


	/**
	 * Scroll pane for data table.
	 *
	 * @since 0.17.0
	 */
	@FXML
	private ScrollPane scrDataTable;

	/**
	 * Map of available data tables.
	 *
	 * @since 0.17.0
	 */
	private static Map<String, Map.Entry<Parent, FXMLLoader>> mapDataTableViews = null;

	/**
	 * Current data tables.
	 *
	 * @since 0.17.0
	 */
	private static String sCurrentDataTable = null;


	/**
	 * Status bar: selection.
	 *
	 * @since 0.17.0
	 */
	@FXML
	private Label lblStatusProgress;

	/**
	 * Status bar: data table.
	 *
	 * @since 0.17.0
	 */
	@FXML
	private Label lblStatusDataTable;

	/**
	 * Status bar: test mail.
	 *
	 * @since 0.17.0
	 */
	@FXML
	private Label lblStatusTestMail;


	/**
	 * Main app controller.
	 */
	private AppLayoutController appController = null;


	/**
	 * Data table controller.
	 */
	private AbstractDataTableController<TitledIDTypeModel> ctlRecipientList;

	/**
	 * Current data features of the loaded data table.
	 */
	private Map<DataFeature, BooleanProperty> mapCurrentDataFeature = null;

	/**
	 * Tasks.
	 */
	private Map<String, IDocumentGenerationTask> mapDocumentGenerationTasks = null;

	/**
	 * Additional input elements.
	 */
	private Map<String, VBox> mapAdditionalInputElements = null;

	/**
	 * Expression needed, because initialization of tasks has to be done before generation button disable property can be set.
	 */
	private BooleanExpression exprDisableStartGenerationButton = null;

	/**
	 * Initializes the controller class.
	 *
	 * This method is automatically called after the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {

		// communication file
		txtMessageFilename.setText(LocalPrefs.get(PrefKey.REFEREE_COMMUNICATION_FILE));
		txtMessageFilename.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
			LocalPrefs.put(PrefKey.REFEREE_COMMUNICATION_FILE, (newValue == null) ? "" : newValue);
		});

		// initialize map of data features
		mapCurrentDataFeature = new HashMap<>();
		for (DataFeature theDataFeature : DataFeature.values()) {
			mapCurrentDataFeature.put(theDataFeature, new SimpleBooleanProperty(false));
		}

		// task ui
		buildTaskUI();

		// icons/texts
		ControlUtils.fillViewControl(btnStartGeneration, this, Prefs.SIZE_BUTTON_SMALL, ResourceType.ICON, ResourceType.TEXT);
		ControlUtils.fillViewControl(btnDataTableSelect, this, Prefs.SIZE_BUTTON_SMALL, ResourceType.ICON);
		ControlUtils.fillViewControl(btnPrefs, this, Prefs.SIZE_BUTTON_SMALL, ResourceType.ICON);

		ControlUtils.fillViewControls(ResourceType.TEXT, this,
				lblMessage,
				lblTitle,
				lblSubtitle,
				lblOpening,
				lblBody,
				lblClosing,
				lblSignature,

				lblTechnical,
				lblDate,
				lblFilename,
				lblAttachments,
				lblMessageFile,
				lblMessageFilename,
				lblOutputPath,
				lblOptions,

				lblDocumentGenerationType
				);

		TableColumnUtils.fillTableColumn(colAttachmentTitle, this, ResourceType.TEXT);
		TableColumnUtils.fillTableColumn(colAttachmentFilename, this, ResourceType.TEXT);
		TableColumnUtils.fillTableColumn(colAttachmentLandscape, this, ResourceType.TEXT);
		ControlUtils.fillViewControl(btnAttachmentAdd, this, Prefs.SIZE_BUTTON_SMALL, ResourceType.ICON);
		ControlUtils.fillViewControl(btnAttachmentEdit, this, Prefs.SIZE_BUTTON_SMALL, ResourceType.ICON);
		ControlUtils.fillViewControl(btnAttachmentDelete, this, Prefs.SIZE_BUTTON_SMALL, ResourceType.ICON);

		ControlUtils.fillViewControl(btnMessageFileSelect, this, Prefs.SIZE_BUTTON_SMALL, ResourceType.ICON);
		ControlUtils.fillViewControl(btnMessageFileLoad, this, Prefs.SIZE_BUTTON_SMALL, ResourceType.ICON);
		ControlUtils.fillViewControl(btnMessageFileSave, this, Prefs.SIZE_BUTTON_SMALL, ResourceType.ICON);

		// enabling buttons
		ObservableBooleanValue isEmptyFilename = txtMessageFilename.textProperty().isEmpty();
		btnMessageFileLoad.disableProperty().bind(isEmptyFilename);
		btnMessageFileSave.disableProperty().bind(isEmptyFilename);
		ObservableBooleanValue isNoAttachmentSelected = tblAttachments.getSelectionModel().selectedItemProperty().isNull();
		btnAttachmentEdit.disableProperty().bind(isNoAttachmentSelected);
		btnAttachmentDelete.disableProperty().bind(isNoAttachmentSelected);

		// attachment list
		colAttachmentFilename.setCellValueFactory(cellData -> cellData.getValue().getFilename());
		colAttachmentTitle.setCellValueFactory(cellData -> cellData.getValue().getTitle());
		colAttachmentLandscape.setCellValueFactory(cellData -> cellData.getValue().getLandscape());
		colAttachmentLandscape.setCellFactory(col -> new CheckBoxTableCell<>());

		Label lblPlaceholder = new Label(I18N.getViewNodeText(this, "attachments.placeholder", ResourceType.TEXT).orElse("?"));
		lblPlaceholder.setWrapText(true);
		tblAttachments.setPlaceholder(lblPlaceholder);

		tblAttachments.setItems(FXCollections.observableList(new ArrayList<>()));

	}

	/**
	 * Returns document data controls mapping.
	 *
	 * TODO Map.of(...) can only add up to 10 K/V pairs, thus I am using the entry adding method
	 *
	 * TODO potential singleton
	 *
	 * @return document data controls mapping
	 */
	private Map<MessageTextElement, Set<Control>> getDocumentDataControlsMapping() {

		return Map.ofEntries(
				Map.entry(MessageTextElement.ATTACHMENT, Set.of(lblAttachments, tblAttachments, barAttachments)),
				Map.entry(MessageTextElement.BODY, Set.of(lblBody, txtBody)),
				Map.entry(MessageTextElement.CLOSING, Set.of(lblClosing, txtClosing)),
				Map.entry(MessageTextElement.DATE, Set.of(lblDate, txtDate)),
				Map.entry(MessageTextElement.FILENAME, Set.of(lblFilename, txtFilename)),
				Map.entry(MessageTextElement.OPENING, Set.of(lblOpening, txtOpening)),
				Map.entry(MessageTextElement.OPTIONS, Set.of(lblOptions, txtOptions)),
				Map.entry(MessageTextElement.OUTPUTPATH, Set.of(lblOutputPath, txtOutputPath)),
				Map.entry(MessageTextElement.SIGNATURE, Set.of(lblSignature, txtSignature)),
				Map.entry(MessageTextElement.SUBTITLE, Set.of(lblSubtitle, txtSubtitle)),
				Map.entry(MessageTextElement.TITLE, Set.of(lblTitle, txtTitle))
				);

	}

	/**
	 * Returns document data controls mapping with text input controls only (one per element).
	 *
	 * TODO potential singleton
	 *
	 * @return document data controls mapping, text input controls only
	 */
	private Map<MessageTextElement, TextInputControl> getDocumentDataControlsMappingInputControls() {

		// TODO see if this could be done with a lambda for the map as well
		Map<MessageTextElement, TextInputControl> mapReturn = new HashMap<>();

		getDocumentDataControlsMapping().forEach((element, controlset) -> {
			controlset.stream()
					.filter(control -> control instanceof TextInputControl)
					.findFirst()
					.ifPresent(control -> mapReturn.put(element, (TextInputControl) control));
		});

		return mapReturn;

	}

	/**
	 * Builds and connects the task ui from defined tasks.
	 */
	private void buildTaskUI() {

		mapDocumentGenerationTasks = new HashMap<>();
		mapAdditionalInputElements = new HashMap<>();
		grpDocumentGenerationType = new ToggleGroup();
		exprDisableStartGenerationButton = new SimpleBooleanProperty(false);

		Map<MessageTextElement, BooleanExpression> mapDocumentDataVisibility = new HashMap<>();

		getDocumentGenerationTasks().stream().forEach(task -> {

			mapDocumentGenerationTasks.put(task.getID(), task);

			RadioButton radTaskSelector = new RadioButton();
			radTaskSelector.setId(task.getID());
			radTaskSelector.setToggleGroup(grpDocumentGenerationType);
			radTaskSelector.setText(task.getTitle());

			boxDocumentGenerationType.getChildren().add(radTaskSelector);

			task.getRequiredDataFeatures().ifPresent(list -> {
				if (!list.isEmpty()) {
					BooleanExpression exprTemp = new SimpleBooleanProperty(false);
					for (DataFeature theDataFeature : list) {
						exprTemp = exprTemp.or(mapCurrentDataFeature.get(theDataFeature));
					}
					radTaskSelector.visibleProperty().bind(exprTemp);
					radTaskSelector.managedProperty().bind(exprTemp);
				}
			});

			task.getAdditionalInputElements().ifPresent(vbox -> {
				boxDocumentGenerationTasks.getChildren().add(vbox);
				vbox.visibleProperty().bind(radTaskSelector.selectedProperty().and(radTaskSelector.visibleProperty()));
				vbox.managedProperty().bind(radTaskSelector.selectedProperty().and(radTaskSelector.managedProperty()));
				mapAdditionalInputElements.put(task.getID(), vbox);
			});

			task.getAllDocumentData().forEach(it -> {
				mapDocumentDataVisibility.putIfAbsent(it, new SimpleBooleanProperty(false));
				mapDocumentDataVisibility.put(it, mapDocumentDataVisibility.get(it).or(radTaskSelector.selectedProperty()));
			});

			if (!task.getMandatoryDocumentData().isEmpty()) {
				BooleanExpression exprTemp = new SimpleBooleanProperty(false);
				for (MessageTextElement theDataElement : task.getMandatoryDocumentData()) {
					exprTemp = exprTemp.or(getDocumentDataControlsMappingInputControls().get(theDataElement).textProperty().isEmpty());
				}
				exprDisableStartGenerationButton = exprDisableStartGenerationButton.or(radTaskSelector.selectedProperty().and(exprTemp));
			}

			radTaskSelector.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
				if (newValue) {
					getDocumentDataControlsMapping().entrySet().forEach(it -> {
						it.getValue().stream()
							.filter(control -> control instanceof Label)
							.forEach(control -> {
								if (task.getMandatoryDocumentData().contains(it.getKey())) {
									((Label) control).getStyleClass().add(CSSClasses.REQUIRED.value());
								} else {
									((Label) control).getStyleClass().remove(CSSClasses.REQUIRED.value());
								}
							});
					});
				}
			});

		});

		// visibility of data controls
		mapDocumentDataVisibility.entrySet().forEach(it -> {
			getDocumentDataControlsMapping().get(it.getKey()).forEach(control -> {
				control.visibleProperty().bind(it.getValue());
				control.managedProperty().bind(it.getValue());
			});
		});

	}

	/**
	 * Initializes the controller with things that cannot be done during {@link #initialize()}.
	 *
	 * TODO: there has to be a better way of mapping datatable resource keys
	 *
	 * @param theAppController app controller
	 * @param theDataTableViews available data table views
	 * @param theDataTableView view name of active data table
	 */
	public void initController(
			final AppLayoutController theAppController,
			final Map<String, Map.Entry<Parent, FXMLLoader>> theDataTableViews,
			final String theDataTableView
			) {

		appController = theAppController;

		mapDataTableViews = theDataTableViews;

		// show selected data table
		loadDataTable(theDataTableView);

		// set divider position
		pneSplit.setDividerPositions(Double.parseDouble(LocalPrefs.get(PrefKey.REFEREE_COMMUNICATION_SPLIT_0)), Double.parseDouble(LocalPrefs.get(PrefKey.REFEREE_COMMUNICATION_SPLIT_1)));

		// if changed, save divider position to preferences
		pneSplit.getDividers().get(0).positionProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
			LocalPrefs.put(PrefKey.REFEREE_COMMUNICATION_SPLIT_0, Double.toString(newValue.doubleValue()));
		});
		pneSplit.getDividers().get(1).positionProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
			LocalPrefs.put(PrefKey.REFEREE_COMMUNICATION_SPLIT_1, Double.toString(newValue.doubleValue()));
		});

		// enabling generation button
		btnStartGeneration.disableProperty().bind(
				ctlRecipientList.selectedItemProperty().isNull()
				.or(exprDisableStartGenerationButton)
		);

		// set data table items
		ctlRecipientList.setDataTableItems();

		updateStatusBar();

	}

	/**
	 * Returns selected task.
	 *
	 * @return selected task
	 */
	private IDocumentGenerationTask getSelectedTask() {
		return getTaskByID(((RadioButton) grpDocumentGenerationType.getSelectedToggle()).getId());
	}

	/**
	 * Returns task with given id.
	 *
	 * @param theID id of task
	 * @return task with id
	 */
	private IDocumentGenerationTask getTaskByID(
			final String theID
			) {
		return mapDocumentGenerationTasks.get(theID);
	}

	/**
	 * Handle document generation start button: start single operation.
	 */
	@FXML
	private void handleStartGeneration() {

		ObjectFactory docGenFactory = new ObjectFactory();

		DocumentGenerationQueue docGenQueue = docGenFactory.createDocumentGenerationQueue();

		Info info = new de.edgesoft.edgeutils.commons.ObjectFactory().createInfo();

		info.setCreated(LocalDateTime.now());
		info.setModified(LocalDateTime.now());
		info.setAppversion(RefereeManager.getVersion());
		info.setDocversion(RefereeManager.getVersion());
		info.setCreator(RefereeManager.class.getCanonicalName());

		docGenQueue.setInfo(info);


		DocumentGenerationJob docGenJob = DocumentGenerationJobModel.createInstance();

		docGenJob.setTaskID(new SimpleStringProperty(getSelectedTask().getID()));
		docGenJob.setMessageFilename(new SimpleStringProperty(Strings.trimToNull(txtMessageFilename.getText())));
		docGenJob.setMessageFileOutputPath(new SimpleStringProperty(Strings.trimToNull(txtOutputPath.getText())));
		docGenJob.setMessageFileOptions(new SimpleStringProperty(Strings.trimToNull(txtOptions.getText())));

		if (mapAdditionalInputElements.containsKey(getSelectedTask().getID())) {
			docGenJob.getTaskInput().putAll(getTaskInput(mapAdditionalInputElements.get(getSelectedTask().getID())));
		}

		docGenQueue.getDocumentGenerationJob().add(docGenJob);


		processGenerationQueue(docGenQueue);

	}

	/**
	 * Returns task input.
	 *
	 * @param theParent parent element
	 * @return task input map
	 */
	private static Map<String, SimpleStringProperty> getTaskInput(
			final Parent theParent
			) {

		Map<String, SimpleStringProperty> mapReturn = new HashMap<>();

		theParent.getChildrenUnmodifiable().forEach(child -> {

			Optional<String> sValue = Optional.empty();

			if (child instanceof TextInputControl) {
				sValue = Optional.of(((TextInputControl) child).getText());
			}
			if (child instanceof CheckBox) {
				sValue = Optional.of(Boolean.toString(((CheckBox) child).isSelected()));
			}

			if (sValue.isPresent()) {
				mapReturn.put(child.getId(), new SimpleStringProperty(sValue.get()));
			}


			if (child instanceof Parent) {
				mapReturn.putAll(getTaskInput((Parent) child));
			}

		});

		return mapReturn;

	}

	/**
	 * Handle document generation start button: start single operation.
	 */
	private void processGenerationQueue(
			final DocumentGenerationQueue theGenQueue
			) {

		// no try-with-resource here because of the task call which does not close the writer correctly
		try {

			Writer wrtProtocol = AppenderUtils.startWriterAppender(DOCUMENT_GENERATION_APPENDER);

			LocalTime tmeStart = LocalTime.now();
			RefereeManager.logger.info(MessageFormat.format("Starte Abarbeitung der Generierungs-Queue: {0,choice,0#kein Task|1#ein Task|1<are {0,number,integer} Tasks}.",
					theGenQueue.getDocumentGenerationJob().size()));

			// TODO: this code does not work for more than 1 job for now, see issue #83
			for (DocumentGenerationJob theGenerationJob : theGenQueue.getDocumentGenerationJob()) {

				IDocumentGenerationTask currentTask = getTaskByID(theGenerationJob.getTaskID().getValue());

				RefereeManager.logger.info(MessageFormat.format("Generierungstyp: {0}", currentTask.getTitle()));

				boolean startGeneration = true;
				if (currentTask.getRequiredDataFeatures().isPresent() && currentTask.getRequiredDataFeatures().get().contains(DataFeature.HAS_EMAIL)) {

					Alert alert = AlertUtils.createAlert(AlertType.CONFIRMATION, appController.getPrimaryStage(),
							"Bestätigung E-Mail senden",
							"E-Mails versenden?",
							"Je nach Empfängeranzahl kann das etwas dauern.");

					Optional<ButtonType> result = alert.showAndWait();
					startGeneration = (result.isPresent() && (result.get() == ButtonType.OK));

				}

				if (startGeneration) {

					Path pathMessageFile = Paths.get(theGenerationJob.getMessageFilename().getValue());
					RefereeManager.logger.info(MessageFormat.format("Lese Nachricht ''{0}''.", pathMessageFile.toAbsolutePath().toString()));
					theGenerationJob.setMessageContent(new SimpleStringProperty(FileAccess.readFile(pathMessageFile)));
					RefereeManager.logger.info("Nachricht erfolgreich gelesen.");

					RefereeManager.logger.info("Erzeuge Generierungstask.");
					Task<Map<DocumentGenerationResult, Integer>> taskGeneration = currentTask.getDocumentGenerationTask(theGenerationJob, ctlRecipientList);

			        // progress dialog
					Map.Entry<Parent, FXMLLoader> pneLoad = Resources.loadNode("ProgressDialog");

					Stage dialogStage = new Stage(StageStyle.UTILITY);
					dialogStage.initModality(Modality.APPLICATION_MODAL);
					dialogStage.setResizable(false);
					dialogStage.setScene(SceneUtils.createScene(pneLoad.getKey(), RefereeManager.CSS));

					ProgressDialogController controller = pneLoad.getValue().getController();
					controller.initController(currentTask.getTitle(), taskGeneration);

			        // task succeeded - show results
			        taskGeneration.setOnSucceeded(event -> {

			            dialogStage.close();
			            showGenerationResult(event, tmeStart, taskGeneration.getValue(), wrtProtocol.toString());

			        });

			        taskGeneration.setOnCancelled(event -> {

			            dialogStage.close();
			            // TODO Message?
						RefereeManager.logger.error(event.getSource().getException());
			            showGenerationResult(event, tmeStart, taskGeneration.getValue(), String.join(System.lineSeparator(), wrtProtocol.toString(), event.getSource().getMessage()));

			        });

			        taskGeneration.setOnFailed(event -> {

			            dialogStage.close();
						RefereeManager.logger.error(event.getSource().getException());
			            showGenerationResult(event, tmeStart, taskGeneration.getValue(), String.join(System.lineSeparator(), wrtProtocol.toString(), ThrowableUtils.getStackTrace(event.getSource().getException())));

			        });

			        dialogStage.show();

					RefereeManager.logger.info("Rufe Generierungstask auf.");
			        Thread thread = new Thread(taskGeneration);
			        thread.start();

				} else {
					RefereeManager.logger.info("Dokumentgenerierung abgebrochen.");
					AppenderUtils.stopAppender(DOCUMENT_GENERATION_APPENDER);
				}

			}

		} catch (Exception e) {

			RefereeManager.logger.error(e);
			e.printStackTrace();

			Alert alert = AlertUtils.createExpandableAlert(AlertType.ERROR,
					appController.getPrimaryStage(),
					"Dokumentgenerierung",
					"Fehler bei der Dokumentgenerierung.",
					MessageFormat.format("Fehlermeldung: {0}.", e.getLocalizedMessage()),
					"Details:",
					ThrowableUtils.getStackTrace(e));

			alert.showAndWait();

			AppenderUtils.stopAppender(DOCUMENT_GENERATION_APPENDER);

		}

	}

	/**
	 * Show generation result.
	 *
	 * @param theEvent worker event
	 * @param theStartTime start time
	 * @param theResult result
	 * @param theDetails detail message
	 */
	private void showGenerationResult(
			final WorkerStateEvent theEvent,
			final LocalTime theStartTime,
			final Map<DocumentGenerationResult, Integer> theResult,
			final String theDetails
			) {

		RefereeManager.logger.info("Ende Dokumentgenerierung.");
		RefereeManager.logger.info(MessageFormat.format("Dauer: {0}", DateTimeFormatter.ISO_LOCAL_TIME.format(LocalTime.ofSecondOfDay(Duration.between(theStartTime, LocalTime.now()).getSeconds()))));

		// tweaking the result map to avoid null pointer exceptions
		Map<DocumentGenerationResult, Integer> mapResult = (theResult == null) ? new HashMap<>() : new HashMap<>(theResult);
		mapResult.putIfAbsent(DocumentGenerationResult.SUCCESS, 0);
		mapResult.putIfAbsent(DocumentGenerationResult.ERROR, 0);

		AlertType alertType = AlertType.INFORMATION;
		String sHeader = "Generierung erfolgreich.";
		if (theEvent.getEventType() == WorkerStateEvent.WORKER_STATE_CANCELLED) {
			alertType = AlertType.WARNING;
			sHeader = "Generierung abgebrochen.";
		}
		if ((theEvent.getEventType() == WorkerStateEvent.WORKER_STATE_FAILED) || (mapResult.get(DocumentGenerationResult.ERROR) > 0)) {
			alertType = AlertType.ERROR;
			mapResult.compute(DocumentGenerationResult.ERROR, (k, v) -> v + 1);
			sHeader = "Generierung fehlerhaft.";
		}

		Alert alert = AlertUtils.createExpandableAlert(alertType,
				appController.getPrimaryStage(),
				"Dokumentgenerierung",
				sHeader,
				MessageFormat.format("{0,choice,0#Keine Dokumente wurden|1#Ein Dokument wurde|1<{0,number,integer} Dokumente wurden} generiert, es {1,choice,0#traten keine|1#trat ein|1<traten {1,number,integer}} Fehler auf.",
						mapResult.get(DocumentGenerationResult.SUCCESS),
						mapResult.get(DocumentGenerationResult.ERROR)),
				"Details:",
				theDetails);

		alert.showAndWait();

		AppenderUtils.stopAppender(DOCUMENT_GENERATION_APPENDER);

	}

	/**
	 * Open preferences.
	 */
	@FXML
	private void handlePrefs() {

		Map.Entry<Parent, FXMLLoader> pneLoad = Resources.loadNode("PreferencesDialog");

		// Create the dialog Stage.
		Stage dialogStage = new Stage();
		dialogStage.setTitle("Einstellungen");
		dialogStage.initModality(Modality.WINDOW_MODAL);
		dialogStage.initOwner(appController.getPrimaryStage());

		dialogStage.setScene(new Scene(pneLoad.getKey()));

		// initialize controller
		PreferencesDialogController controller = pneLoad.getValue().getController();
		controller.initController(
				dialogStage,
				(getSelectedTask().getRequiredDataFeatures().isPresent() && getSelectedTask().getRequiredDataFeatures().get().contains(DataFeature.HAS_EMAIL)) ? "tabEMail" :
					(getSelectedTask().getRequiredDataFeatures().isPresent() && getSelectedTask().getRequiredDataFeatures().get().contains(DataFeature.HAS_ADDRESS)) ? "tabLetters" :
						"tabTexts"
				);

		// Show the dialog and wait until the user closes it
		dialogStage.showAndWait();

		appController.setAppTitle();

	}

	/**
	 * Message file selection.
	 */
	@FXML
	private void handleMessageFileSelect() {

		FileChooser fileChooser = new FileChooser();

		fileChooser.setTitle("Nachrichtendatei öffnen");
		fileChooser.getExtensionFilters().addAll(
				new FileChooser.ExtensionFilter("Nachrichten-Dateien (*.md, *.mmd)", "*.md", "*.mmd"),
				new FileChooser.ExtensionFilter("Textdateien (*.txt)", "*.txt"),
				new FileChooser.ExtensionFilter("CSV-Dateien (*.csv)", "*.csv"),
				new FileChooser.ExtensionFilter("Lektor-Dateien (*.lr)", "*.lr"),
				new FileChooser.ExtensionFilter("Alle Dateien (*.*)", "*.*")
				);

		if (!LocalPrefs.get(PrefKey.REFEREE_COMMUNICATION_FILE).isEmpty()) {
			Path pthFile = Paths.get(LocalPrefs.get(PrefKey.REFEREE_COMMUNICATION_FILE));
			if ((pthFile != null) && (pthFile.getParent() != null)) {
				fileChooser.setInitialDirectory(pthFile.getParent().toFile());
			}
		}

		File file = fileChooser.showOpenDialog(appController.getPrimaryStage());

		if (file != null) {
			txtMessageFilename.setText(file.getPath());
		}

	}

	/**
	 * Message file load.
	 */
	@FXML
	private void handleMessageFileLoad() {

		Alert alert = AlertUtils.createAlert(AlertType.CONFIRMATION, appController.getPrimaryStage(),
				"Bestätigung Nachrichtendatei laden",
				"Soll die Nachrichtendatei geladen werden?",
				"Alle Eingaben werden dabei gelöscht bzw. überschrieben.");

		alert.showAndWait()
				.filter(response -> response == ButtonType.OK)
				.ifPresent(response -> {


			tblAttachments.getItems().clear();
			for (MessageTextElement theTextElement : MessageTextElement.values()) {
				if (getDocumentDataControlsMappingInputControls().containsKey(theTextElement)) {
					getDocumentDataControlsMappingInputControls().get(theTextElement).setText(null);
				}
			}

			try {

				MessageText msgText = MessageTextModel.fromFileString(FileAccess.readFile(Paths.get(txtMessageFilename.getText())));

				tblAttachments.getItems().addAll(msgText.getAttachment());
				for (MessageTextElement theTextElement : MessageTextElement.values()) {
					StringProperty prpValue = msgText.getValues().get(theTextElement);
					if ((prpValue != null) && (prpValue.getValue() != null)) {
						getDocumentDataControlsMappingInputControls().get(theTextElement).setText(prpValue.getValue());
					}
				}

			} catch (Exception e) {

				RefereeManager.logger.error(e);
				e.printStackTrace();

				AlertUtils.createAlert(AlertType.ERROR, appController.getPrimaryStage(),
						"Dateifehler",
						"Ein Fehler ist beim Laden der Nachricht aufgetreten.",
						MessageFormat.format("{0}\nDie Daten wurden nicht geladen.", e.getMessage()))
				.showAndWait();

			}

		});

	}

	/**
	 * Message file save.
	 *
	 * TODO save message file
	 */
	@FXML
	private void handleMessageFileSave() {

		RefereeManager.logger.warn("#handleMessageFileSave");

		MessageText msgContent = MessageTextModel.createInstance();

		for (MessageTextElement theTextElement : MessageTextElement.values()) {
			msgContent.getValues().put(theTextElement, new SimpleStringProperty(getDocumentDataControlsMappingInputControls().get(theTextElement).getText()));
		}

		tblAttachments.getItems().forEach(it -> {
			msgContent.getAttachment().add(it);
		});

	}

	/**
	 * Attachment addition.
	 */
	@FXML
	private void handleAttachmentAdd() {

		Attachment newAttachment = new de.edgesoft.refereemanager.messagetext.jaxb.ObjectFactory().createAttachment();
		if (showAttachmentEditDialog(newAttachment)) {
			tblAttachments.getItems().add(newAttachment);
			tblAttachments.getSelectionModel().select(newAttachment);
		}

	}

	/**
	 * Attachment edit.
	 */
	@FXML
	private void handleAttachmentEdit() {

		Attachment editAttachment = tblAttachments.getSelectionModel().getSelectedItem();

		if (editAttachment != null) {
			showAttachmentEditDialog(editAttachment);
		}

	}

	/**
	 * Attachment deletion.
	 */
	@FXML
	private void handleAttachmentDelete() {

		Attachment selectedAttachment = tblAttachments.getSelectionModel().getSelectedItem();

		if (selectedAttachment != null) {

			Alert alert = AlertUtils.createAlert(AlertType.CONFIRMATION, appController.getPrimaryStage(),
					"Bestätigung Attachment löschen",
					"Soll das ausgewählte Attachment gelöscht werden?",
					null);

			alert.showAndWait()
					.filter(response -> response == ButtonType.OK)
					.ifPresent(response -> {
						tblAttachments.getItems().remove(selectedAttachment);
						});

		}

	}

	/**
	 * Opens the attachment edit dialog.
	 *
	 * @param theAttachment the attachment to be edited
	 * @return true if the user clicked OK, false otherwise.
	 */
	private boolean showAttachmentEditDialog(Attachment theAttachment) {

		Map.Entry<Parent, FXMLLoader> pneLoad = Resources.loadNode("AttachmentEditDialog");

		// Create the dialog Stage.
		Stage dialogStage = new Stage();
		dialogStage.initModality(Modality.WINDOW_MODAL);
		dialogStage.initOwner(appController.getPrimaryStage());
		dialogStage.setTitle("Dateianhang editieren");

		dialogStage.setScene(SceneUtils.createScene(pneLoad.getKey(), RefereeManager.CSS));

		// Set the attachment
		AttachmentEditDialogController editController = pneLoad.getValue().getController();
		editController.setDialogStage(dialogStage);
		editController.setAttachment(theAttachment);

		// Show the dialog and wait until the user closes it
		dialogStage.showAndWait();

		return editController.isOkClicked();

	}

	/**
	 * Loads and shows the selected data table.
	 *
	 * @param theDataTableView view name of active data table
	 */
	private void loadDataTable(
			final String theDataTableView
			) {

		scrDataTable.setContent(mapDataTableViews.get(theDataTableView).getKey());
		ctlRecipientList = mapDataTableViews.get(theDataTableView).getValue().getController();
		ctlRecipientList.setSelectionMode(SelectionMode.MULTIPLE);

		for (DataFeature theDataFeature : DataFeature.values()) {
			mapCurrentDataFeature.get(theDataFeature).setValue(ctlRecipientList.getProvidedDataFeatures().orElse(Collections.emptySet()).contains(theDataFeature));
		}
		sCurrentDataTable = theDataTableView;

		grpDocumentGenerationType.getToggles().stream().filter(it -> ((RadioButton) it).isVisible()).findFirst().ifPresent(it -> it.setSelected(true));
		updateStatusBar();

	}

	/**
	 * Open data table select dialog.
	 *
	 * Not static, because otherwise FXML can not initialize the controller correctly.
	 */
	@FXML
	private void handleDataTableSelect() {

		FlowPane pneSelection = new FlowPane();
		pneSelection.getStyleClass().add("refereemanager-datatableselect");

	    // prepare data table selection dialog
		for (Entry<String, Entry<Parent, FXMLLoader>> theDataTable : mapDataTableViews.entrySet()) {

			Button btnDataTable = new Button();
			double dSize = Resources.getScalingFactor() * Prefs.SIZE_ALERT * 2;
			btnDataTable.setPrefSize(dSize, dSize);
			btnDataTable.setContentDisplay(ContentDisplay.TOP);
			btnDataTable.setWrapText(true);
			btnDataTable.setTextAlignment(TextAlignment.CENTER);

			I18N.getText(createDataTableKey(theDataTable.getKey(), ResourceType.ICON)).ifPresent(it -> btnDataTable.setGraphic(new ImageView(Resources.loadImage(it, Prefs.SIZE_ALERT))));
			I18N.getText(createDataTableKey(theDataTable.getKey(), ResourceType.TEXT)).ifPresent(it -> {
						btnDataTable.setText(it);
						btnDataTable.setTooltip(TooltipUtils.createTooltip(it));
					});

			btnDataTable.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					event.consume();
					loadDataTable(theDataTable.getKey());
					((Stage) btnDataTable.getScene().getWindow()).close();
				}
			});

			pneSelection.getChildren().add(btnDataTable);

		}

		// prepare stage
		Stage dialogStage = new Stage();
		dialogStage.setTitle(I18N.getText("view.documentgeneration.datatableselect.text").get());
		dialogStage.initOwner(appController.getPrimaryStage());
		dialogStage.initModality(Modality.WINDOW_MODAL);
		dialogStage.setScene(SceneUtils.createScene(new ScrollPane(pneSelection), RefereeManager.CSS));
		dialogStage.setResizable(true);

		// close stage on ESC
		dialogStage.addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {
			@Override
			public void handle(final KeyEvent event){
				if (event.getCode() == KeyCode.ESCAPE) {
					event.consume();
					dialogStage.close();
				}
			}
		});

		// Show the dialog and wait until the user closes it
		dialogStage.show();

	}

	/**
	 * Creates key for given data table.
	 *
	 * TODO: is replacing the key value an elegant solution?
	 *
	 * @param theDataTableView view name of active data table
	 * @return key for given data table
	 */
	private static String createDataTableKey(
			final String theDataTableView,
			final ResourceType theResourceType
			) {

		return I18N.createKey(Resources.PATH_VIEW, "datatables", theDataTableView, theResourceType.value())
				.replace("datatables.datatable", "applayout.overview");

	}

	/**
	 * Updates status bar.
	 */
	private void updateStatusBar() {

		lblStatusProgress.setText(I18N.getViewNodeText(this, ControlUtils.getIdWithoutPrefix(lblStatusProgress), ResourceType.TEXT).get());

		lblStatusDataTable.setText(LabeledUtils.removeMnemonicMarker(MessageFormat.format(I18N.getViewNodeText(this, ControlUtils.getIdWithoutPrefix(lblStatusDataTable), ResourceType.TEXT).get(),
				I18N.getText(createDataTableKey(sCurrentDataTable, ResourceType.TEXT)).get()
		)));

	}

	/**
	 * Returns list of document generation tasks.
	 *
	 * TODO list will later be dynamic using plugins
	 *
	 * @return list of document generation tasks
	 */
	private static List<IDocumentGenerationTask> getDocumentGenerationTasks() {

		return List.of(
				new EMailGenerationTask(),
				new LetterGenerationTask(),
				new MultipleDocumentsGenerationTask(),
				new MultipleTextsGenerationTask(),
				new SingleDocumentGenerationTask(),
				new SingleTextGenerationTask()
				);

	}

}

/* EOF */
