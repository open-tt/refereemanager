package de.edgesoft.refereemanager.controller;

import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;

/**
 * Controller for progress dialog scene.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.10.0
 */
public class ProgressDialogController {

	/**
	 * Title label
	 */
	@FXML
	private Label lblTitle;

	/**
	 * Message label.
	 */
	@FXML
	private Label lblMessage;

	/**
	 * Progress bar.
	 */
	@FXML
	private ProgressBar progBar;

	/**
	 * Initializes the controller class.
	 *
	 * This method is automatically called after the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {

		progBar.setProgress(0);
		progBar.progressProperty().unbind();
		lblMessage.textProperty().unbind();

	}

	/**
	 * Initializes the controller with things that cannot be done during {@link #initialize()}.
	 *
	 * @param theTitle title
	 * @param theTask task to bind
	 */
	public void initController(
			final String theTitle,
			final Task<?> theTask
			) {

		lblTitle.setText(theTitle);
		progBar.progressProperty().bind(theTask.progressProperty());
		lblMessage.textProperty().bind(theTask.messageProperty());

	}

}

/* EOF */
