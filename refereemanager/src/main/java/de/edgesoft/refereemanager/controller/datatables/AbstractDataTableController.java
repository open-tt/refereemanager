package de.edgesoft.refereemanager.controller.datatables;

import java.text.MessageFormat;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import de.edgesoft.edgeutils.i18n.ResourceType;
import de.edgesoft.edgeutils.javafx.ControlUtils;
import de.edgesoft.refereemanager.documentgeneration.DataFeature;
import de.edgesoft.refereemanager.model.TitledIDTypeModel;
import de.edgesoft.refereemanager.utils.TableUtils;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.MultipleSelectionModel;

/**
 * Abstract controller for data table scenes.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.15.0
 */
public abstract class AbstractDataTableController<T extends TitledIDTypeModel> extends AbstractListController<T> implements IDataTableController<T> {

	/**
	 * Label status message.
	 *
	 * @since 0.16.0
	 */
	@FXML
	protected Label lblStatus;

	/**
	 * Initializes the controller class.
	 *
	 * This method is automatically called after the fxml file has been loaded.
	 */
	@FXML
	protected void initialize() {

		ControlUtils.fillViewControls(ResourceType.TEXT, this,
				lblStatus
				);

		getSelectionModel().selectedItemProperty().addListener((value, oldSelection, newSelection) -> updateTable());

	}

	/**
	 * Returns selection model.
	 *
	 * @return selection model
	 */
	@Override
	public MultipleSelectionModel<T> getSelectionModel() {
		return getDataTable().getSelectionModel();
	}

	/**
	 * Sets table placeholder noun.
	 *
	 * @param thePlaceholderNoun placeholder noun
	 */
	@Override
	public void setDataTablePlaceholderNoun(final String thePlaceholderNoun) {

		Label lblPlaceholder = new Label(MessageFormat.format(
				(getDataTable().getItems().size() == 0) ? TableUtils.TABLE_NO_DATA : TableUtils.TABLE_FILTERED,
				thePlaceholderNoun));
		lblPlaceholder.setWrapText(true);
		getDataTable().setPlaceholder(lblPlaceholder);

	}

	/**
	 * Updates status information of data table and refresh it.
	 */
	public void updateTable() {

		lblStatus.setText(MessageFormat.format("({0} angezeigt, {1} ausgewählt)", getDataTable().getItems().size(), getSelectionModel().getSelectedItems().size()));
		getDataTable().refresh();

	}

	@Override
	public List<T> getSelectedItemsAsDisplayed() {
		return getSelectionModel().getSelectedItems();
	}

	@Override
	public Optional<Set<DataFeature>> getProvidedDataFeatures() {
		return Optional.empty();
	}

}

/* EOF */
