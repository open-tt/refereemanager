package de.edgesoft.refereemanager.controller.datatables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import de.edgesoft.edgeutils.i18n.ResourceType;
import de.edgesoft.edgeutils.javafx.TableColumnUtils;
import de.edgesoft.refereemanager.jaxb.EventDateType;
import de.edgesoft.refereemanager.jaxb.Tournament;
import de.edgesoft.refereemanager.model.AppModel;
import de.edgesoft.refereemanager.model.ContentModel;
import de.edgesoft.refereemanager.model.EventDateModel;
import de.edgesoft.refereemanager.model.TitledIDTypeModel;
import de.edgesoft.refereemanager.utils.BooleanFlagCell;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.geometry.Orientation;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Separator;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * Controller for the tournaments list scene.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.15.0
 */
public class DataTableTournamentsController extends AbstractDataTableController<Tournament> {

	/**
	 * Container.
	 */
	@FXML
	private VBox boxContainer;

	/**
	 * Table other events.
	 */
	@FXML
	private TableView<Tournament> tblData;

	/**
	 * ID column.
	 */
	@FXML
	private TableColumn<EventDateModel, String> colID;

	/**
	 * Title column.
	 */
	@FXML
	private TableColumn<EventDateModel, String> colTitle;

	/**
	 * Date column.
	 */
	@FXML
	private TableColumn<EventDateModel, String> colDate;

	/**
	 * Column of highest referee assignment.
	 */
	@FXML
	private TableColumn<EventDateModel, String> colHighest;

	/**
	 * Type column.
	 */
	@FXML
	private TableColumn<EventDateModel, String> colType;

	/**
	 * Referee count column.
	 */
	@FXML
	private TableColumn<EventDateModel, Boolean> colRefereeCount;

	/**
	 * Referee report column.
	 */
	@FXML
	private TableColumn<EventDateModel, Boolean> colRefereeReport;

	/**
	 * Takes place column.
	 */
	@FXML
	private TableColumn<EventDateModel, Boolean> colTakesPlace;


	/**
	 * Filter storage.
	 */
	private Map<CheckBox, EventDateType> mapEventFilterTypes;


	/**
	 * List of other events.
	 */
	private FilteredList<Tournament> lstTournaments;


	/**
	 * Initializes the controller class.
	 *
	 * This method is automatically called after the fxml file has been loaded.
	 */
	@FXML
	@Override
	protected void initialize() {

		super.initialize();

		TableColumnUtils.fillTableColumns(this, ResourceType.TEXT,
				colID,
				colTitle,
				colDate,
				colHighest,
				colType,
				colRefereeCount,
				colRefereeReport,
				colTakesPlace
				);

		// hook data to columns
		colID.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getId()));
		colID.setVisible(false);

		colTitle.setCellValueFactory(cellData -> cellData.getValue().getDisplayTitleShort());
		colDate.setCellValueFactory(cellData -> cellData.getValue().getDateRangeText());
		colHighest.setCellValueFactory(cellData -> cellData.getValue().getRefereeText());
		colType.setCellValueFactory(cellData -> cellData.getValue().getType().getDisplayTitle());

		colRefereeReport.setCellValueFactory(cellData -> cellData.getValue().existsRefereeReportFile());
		colRefereeReport.setCellFactory(new Callback<TableColumn<EventDateModel,Boolean>, TableCell<EventDateModel,Boolean>>() {

			@Override
			public TableCell<EventDateModel, Boolean> call(TableColumn<EventDateModel, Boolean> param) {
				return new BooleanFlagCell<>();
			}

        });

		colRefereeCount.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().isEnoughReferees()));
		colRefereeCount.setCellFactory(new Callback<TableColumn<EventDateModel,Boolean>, TableCell<EventDateModel,Boolean>>() {

			@Override
			public TableCell<EventDateModel, Boolean> call(TableColumn<EventDateModel, Boolean> param) {
				return new BooleanFlagCell<>();
			}

        });

		colTakesPlace.setCellValueFactory(cellData -> cellData.getValue().getCanceled().not());
		colTakesPlace.setCellFactory(new Callback<TableColumn<EventDateModel,Boolean>, TableCell<EventDateModel,Boolean>>() {

			@Override
			public TableCell<EventDateModel, Boolean> call(TableColumn<EventDateModel, Boolean> param) {
				return new BooleanFlagCell<>();
			}

        });

		// setup type filter
		HBox boxTypeFilter = new HBox(5);
		boxContainer.getChildren().add(new Separator(Orientation.HORIZONTAL));
		boxContainer.getChildren().add(boxTypeFilter);

		mapEventFilterTypes = new HashMap<>();
		AppModel.getData().getContent().getEventDateType().stream().sorted(TitledIDTypeModel.SHORTTITLE_TITLE).forEach(
				eventDateType -> {
					CheckBox chkTemp = new CheckBox(eventDateType.getDisplayTitleShort().getValueSafe());
					chkTemp.setOnAction(e -> handleFilterChange());
					boxTypeFilter.getChildren().add(chkTemp);
					mapEventFilterTypes.put(chkTemp, eventDateType);
				}
		);

		// set column title
		colHighest.setText(((ContentModel) AppModel.getData().getContent()).getHighestAssignmentType().getDisplayTitleShort().getValueSafe());

		// init items
		setDataTableItems();

	}

	/**
	 * Returns data table.
	 */
	@Override
	public TableView<Tournament> getDataTable() {
		return tblData;
	}

	/**
	 * Sets table items.
	 */
	@Override
	public void setDataTableItems() {

		lstTournaments = new FilteredList<>(((ContentModel) AppModel.getData().getContent()).getObservableTournaments(), tournament -> true);

		SortedList<Tournament> lstSortedTournaments = new SortedList<>(lstTournaments);
		lstSortedTournaments.comparatorProperty().bind(tblData.comparatorProperty());
		tblData.setItems(lstSortedTournaments);

		setDataTablePlaceholderNoun("Turniere");

		handleFilterChange();

	}

	/**
	 * Handles filter change events.
	 */
	@SuppressWarnings("unchecked")
	@FXML
	@Override
	public void handleFilterChange() {

		// filter for events
		if (lstTournaments != null) {

			lstTournaments.setPredicate(TitledIDTypeModel.ALL);

			for (Entry<CheckBox, EventDateType> entryChkType : mapEventFilterTypes.entrySet()) {

				if (entryChkType.getKey().isSelected()) {
					lstTournaments.setPredicate(((Predicate<Tournament>) lstTournaments.getPredicate()).and(EventDateModel.getTypePredicate(entryChkType.getValue())));
				}

			}

		}

		updateTable();

	}

	/**
	 * Returns selection from table as sorted list.
	 *
	 * @return sorted selection from table
	 */
	@Override
	public ObservableList<Tournament> getSortedSelectedItems() {
		List<Tournament> lstReturn = new ArrayList<>();

		getSelectionModel().getSelectedItems().forEach(data -> lstReturn.add(data));

		return FXCollections.observableList(lstReturn.stream().sorted(EventDateModel.DATE_FIRST).collect(Collectors.toList()));
	}

}

/* EOF */
