package de.edgesoft.refereemanager.controller.datatables;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import de.edgesoft.refereemanager.documentgeneration.DataFeature;
import de.edgesoft.refereemanager.model.TitledIDTypeModel;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;

/**
 * Interface for the data table scene controller.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.15.0
 */
public interface IDataTableController<T extends TitledIDTypeModel> extends IListController<T> {

	/**
	 * Returns data table.
	 */
	public TableView<T> getDataTable();

	/**
	 * Sets table items.
	 */
	public void setDataTableItems();

	/**
	 * Sets table placeholder noun.
	 *
	 * @param thePlaceholderNoun placeholder noun
	 */
	public void setDataTablePlaceholderNoun(final String thePlaceholderNoun);

	/**
	 * Handles filter change events.
	 */
	public void handleFilterChange();

	/**
	 * Returns selection as displayed.
	 *
	 * @return selection as displayed
	 *
	 * @since 0.17.0
	 */
	public List<T> getSelectedItemsAsDisplayed();

	/**
	 * Returns selection as sorted list.
	 *
	 * @return sorted selection
	 */
	public ObservableList<T> getSortedSelectedItems();

	/**
	 * Returns provided data features for this data table.
	 *
	 * @return provided data features
	 */
	public Optional<Set<DataFeature>> getProvidedDataFeatures();

}

/* EOF */
