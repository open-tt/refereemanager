package de.edgesoft.refereemanager.controller.details;

import java.text.MessageFormat;
import java.util.stream.Collectors;

import de.edgesoft.edgeutils.files.Prefs;
import de.edgesoft.edgeutils.files.Resources;
import de.edgesoft.edgeutils.i18n.I18N;
import de.edgesoft.edgeutils.i18n.ResourceType;
import de.edgesoft.edgeutils.javafx.ButtonUtils;
import de.edgesoft.edgeutils.javafx.ControlUtils;
import de.edgesoft.edgeutils.javafx.LabeledUtils;
import de.edgesoft.edgeutils.javafx.controller.AbstractHyperlinkController;
import de.edgesoft.edgeutils.javafx.controller.CopyTextPane;
import de.edgesoft.refereemanager.model.LeagueModel;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;

/**
 * Controller for the league details scene.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.15.0
 */
public class DetailsLeagueController<T extends LeagueModel> extends AbstractHyperlinkController implements IDetailsController<T> {

	@FXML
	private Label lblHeading;
	@FXML
	private Label lblSexType;
	@FXML
	private Label lblRank;
	@FXML
	private Label lblNational;
	@FXML
	private Label lblResultsURL;
	@FXML
	private Label lblRefereeReportURL;
	@FXML
	private Label lblRefereeReportRecipient;
	@FXML
	private Label lblAllowanceDaily;
	@FXML
	private Label lblAllowancePerKM;

	/**
	 * Sex type.
	 */
	@FXML
	private Label lblDisplaySexType;

	/**
	 * Rank.
	 */
	@FXML
	private Label lblDisplayRank;

	@FXML
	private Label lblDisplayNational;

	/**
	 * Results.
	 */
	@FXML
	private Hyperlink lnkDisplayResultsURL;

	/**
	 * Copy results URL.
	 */
	@FXML
	private Button btnCopyResultsURL;

	/**
	 * Referee report.
	 */
	@FXML
	private Hyperlink lnkDisplayRefereeReportURL;

	/**
	 * Copy referee report URL.
	 */
	@FXML
	private Button btnCopyRefereeReportURL;

	/**
	 * Referee report recipients.
	 */
	@FXML
	private Label lblDisplayRefereeReportRecipient;

	/**
	 * Daily allowance.
	 */
	@FXML
	private Label lblDisplayAllowanceDaily;

	/**
	 * Allowance per km.
	 */
	@FXML
	private Label lblDisplayAllowancePerKM;


	/**
	 * Initializes the controller class.
	 *
	 * This method is automatically called after the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {

		// icons
		ControlUtils.fillViewControls(ResourceType.TEXT, this,
				lblHeading,
				lblSexType,
				lblRank,
				lblNational,
				lblResultsURL,
				lblRefereeReportURL,
				lblRefereeReportRecipient,
				lblAllowanceDaily,
				lblAllowancePerKM
				);

		ControlUtils.fillViewControl(btnCopyResultsURL, this, Prefs.SIZE_BUTTON_SMALL, ResourceType.ICON, ResourceType.TOOLTIP);
		ControlUtils.fillViewControl(btnCopyRefereeReportURL, this, Prefs.SIZE_BUTTON_SMALL, ResourceType.ICON, ResourceType.TOOLTIP);

		// buttons
		ButtonUtils.bindDisable(btnCopyResultsURL, lnkDisplayResultsURL);
		ButtonUtils.bindDisable(btnCopyRefereeReportURL, lnkDisplayRefereeReportURL);

	}

	/**
	 * Shows detail data.
	 *
	 * @param theDetailData (null if no data to show)
	 */
	@Override
	public void showDetails(final T theDetailData) {

		if (theDetailData == null) {

			LabeledUtils.setText(lblDisplaySexType, null);
			LabeledUtils.setText(lblDisplayRank, null);
			lblDisplayNational.setGraphic(null);
			lnkDisplayResultsURL.setText(null);
			lnkDisplayRefereeReportURL.setText(null);
			LabeledUtils.setText(lblDisplayRefereeReportRecipient, null);
			LabeledUtils.setText(lblDisplayAllowanceDaily, null);
			LabeledUtils.setText(lblDisplayAllowancePerKM, null);

		} else {

			lblDisplaySexType.setText(
					(theDetailData.getSexType() == null) ?
							null :
							theDetailData.getSexType().getDisplayText().getValue());

			lblDisplayRank.setText(
					(theDetailData.getRank() == null) ?
							null :
							MessageFormat.format("{0,number}", theDetailData.getRank().getValue()));

			lblDisplayNational.setGraphic(new ImageView(Resources.loadImage(
					theDetailData.getNational().getValue() ? I18N.getText("refman.emblem.yes.icon").get() : I18N.getText("refman.emblem.no.icon").get(),
							Prefs.SIZE_FLAG))
					);

			lnkDisplayResultsURL.setText(
					(theDetailData.getResultsURL() == null) ?
							null :
							theDetailData.getResultsURL().getValue());

			lnkDisplayRefereeReportURL.setText(
					(theDetailData.getRefereeReportURL() == null) ?
							null :
							theDetailData.getRefereeReportURL().getValue());

			lblDisplayRefereeReportRecipient.setText(theDetailData.getPerson().stream().map(person -> person.getDisplayText().getValueSafe()).collect(Collectors.joining(System.lineSeparator())));

			lblDisplayAllowanceDaily.setText((theDetailData.getAllowanceDaily().getValue() < 0.0) ? null : MessageFormat.format("{0,number,currency}", theDetailData.getAllowanceDaily().getValue()));
			lblDisplayAllowancePerKM.setText((theDetailData.getAllowancePerKM().getValue() < 0.0) ? null : MessageFormat.format("{0,number,currency}", theDetailData.getAllowancePerKM().getValue()));

		}

	}

	/**
	 * Copy results url.
	 */
	@FXML
	private void handleCopyResultsURL() {
		CopyTextPane.copyText(lnkDisplayResultsURL);
	}

	/**
	 * Copy referee report url.
	 */
	@FXML
	private void handleCopyRefereeReportURL() {
		CopyTextPane.copyText(lnkDisplayRefereeReportURL);
	}

}

/* EOF */
