package de.edgesoft.refereemanager.controller.details;

import java.util.stream.Collectors;

import de.edgesoft.edgeutils.files.Prefs;
import de.edgesoft.edgeutils.files.Resources;
import de.edgesoft.edgeutils.javafx.LabeledUtils;
import de.edgesoft.refereemanager.model.LeagueGameModel;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;

/**
 * Controller for the league game details scene.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.15.0
 */
public class DetailsLeagueGameController<T extends LeagueGameModel> implements IDetailsController<T> {

	/**
	 * Number label.
	 */
	@FXML
	private Label lblNumber;

	/**
	 * Date label.
	 */
	@FXML
	private Label lblDate;

	/**
	 * Time label.
	 */
	@FXML
	private Label lblTime;

	/**
	 * Teams label.
	 */
	@FXML
	private Label lblTeams;

	/**
	 * League label.
	 */
	@FXML
	private Label lblLeague;

	/**
	 * Venue label.
	 */
	@FXML
	private Label lblVenues;

	/**
	 * Referee report label.
	 */
	@FXML
	private Label lblRefereeReport;

	/**
	 * Referee report label label.
	 */
	@FXML
	private Label lblRefereeReportLabel;

	/**
	 * Referee report indicator label.
	 */
	@FXML
	private Label lblRefereeReportIndicator;

	/**
	 * Referee report copy button.
	 */
	@FXML
	private Button btnRefereeReportCopy;

	/**
	 * Type label.
	 */
	@FXML
	private Label lblType;

	/**
	 * Initializes the controller class.
	 *
	 * This method is automatically called after the fxml file has been loaded.
	 */
	@FXML
	protected void initialize() {

		// icons
		btnRefereeReportCopy.setGraphic(new ImageView(Resources.loadImage("icons/actions/edit-copy.svg", Prefs.SIZE_BUTTON_SMALL)));

	}

	/**
	 * Shows detail data.
	 *
	 * @param theDetailData (null if no data to show)
	 */
	@Override
	public void showDetails(final T theDetailData) {

		if (theDetailData == null) {

			LabeledUtils.setText(lblNumber, null);
			LabeledUtils.setText(lblDate, null);
			LabeledUtils.setText(lblTime, null);
			LabeledUtils.setText(lblTeams, null);
			LabeledUtils.setText(lblLeague, null);
			LabeledUtils.setText(lblVenues, null);
			LabeledUtils.setText(lblRefereeReport, null);

			lblRefereeReportIndicator.setGraphic(null);
			btnRefereeReportCopy.setDisable(true);

			LabeledUtils.setText(lblType, null);

		} else {

			LabeledUtils.setText(lblNumber, theDetailData.getGameNumberString());
			LabeledUtils.setText(lblDate, theDetailData.getDateRangeText());
//			LabelUtils.setText(lblTime, theDetailData.getTimeText());
			LabeledUtils.setText(lblTeams, theDetailData.getTeamText());

			lblLeague.setText(
					(theDetailData.getHomeTeam() == null) ?
							null :
							theDetailData.getHomeTeam().getLeague().getDisplayTitle().getValueSafe());

			lblVenues.setText(theDetailData.getVenue().stream().map(venue -> venue.getDisplayText().getValueSafe()).collect(Collectors.joining(System.lineSeparator())));

			LabeledUtils.setText(lblRefereeReport, theDetailData.getRefereeReportFilename());

			if (theDetailData.existsRefereeReportFile().get()) {
				lblRefereeReportIndicator.setGraphic(new ImageView(Resources.loadImage("icons/emblems/emblem-success.svg")));
			} else {
				lblRefereeReportIndicator.setGraphic(new ImageView(Resources.loadImage("icons/emblems/emblem-error.svg")));
			}
			btnRefereeReportCopy.setDisable(false);

			LabeledUtils.setText(lblType, new SimpleStringProperty("ToDo"));

		}

	}

	/**
	 * Copies referee report filename to clipboard.
	 */
	@FXML
	private void handleRefereeReportCopy() {
		ClipboardContent clpContent = new ClipboardContent();
		clpContent.putString(lblRefereeReport.getText());
		Clipboard.getSystemClipboard().setContent(clpContent);
	}

}

/* EOF */
