package de.edgesoft.refereemanager.controller.details;

import java.io.File;
import java.nio.file.Paths;

import de.edgesoft.edgeutils.javafx.LabeledUtils;
import de.edgesoft.edgeutils.javafx.controller.AbstractHyperlinkController;
import de.edgesoft.edgeutils.javafx.controller.CopyTextPane;
import de.edgesoft.refereemanager.RefereeManager;
import de.edgesoft.refereemanager.model.PersonModel;
import de.edgesoft.refereemanager.model.PhoneNumberModel;
import de.edgesoft.refereemanager.utils.LocalPrefs;
import de.edgesoft.refereemanager.utils.PrefKey;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

/**
 * Controller for the person details scene.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.15.0
 */
public class DetailsPersonController<T extends PersonModel> extends AbstractHyperlinkController implements IDetailsController<T> {

	/**
	 * Name.
	 */
	@FXML
	private Label lblName;

	/**
	 * First name.
	 */
	@FXML
	private Label lblFirstName;

	/**
	 * Birthday.
	 */
	@FXML
	private Label lblBirthday;

	/**
	 * Role.
	 */
	@FXML
	private Label lblRole;

	/**
	 * E-Mail box.
	 */
	@FXML
	private VBox boxEMail;

	/**
	 * Phone box.
	 */
	@FXML
	private VBox boxPhoneNumber;

	/**
	 * Address box.
	 */
	@FXML
	private VBox boxAddress;

	/**
	 * Image.
	 */
	@FXML
	private ImageView imgView;


	/**
	 * Shows detail data.
	 *
	 * @param theDetailData (null if no data to show)
	 */
	@Override
	public void showDetails(final T theDetailData) {

		if (theDetailData == null) {

			LabeledUtils.setText(lblName, null);
			LabeledUtils.setText(lblFirstName, null);
			LabeledUtils.setText(lblBirthday, null);
			LabeledUtils.setText(lblRole, null);

			boxEMail.getChildren().clear();
			boxPhoneNumber.getChildren().clear();
			boxAddress.getChildren().clear();

			imgView.setImage(null);

		} else {

			LabeledUtils.setText(lblName, theDetailData.getName());
			LabeledUtils.setText(lblFirstName, theDetailData.getFirstName());
			LabeledUtils.setText(lblBirthday, theDetailData.getBirthday(), null);

			lblRole.setText(
					(theDetailData.getRole() == null) ?
							null :
							theDetailData.getRole().getDisplayText().getValue());

			boxEMail.getChildren().clear();
			theDetailData.getEMail().stream().forEach(email -> boxEMail.getChildren().add(CopyTextPane.createInstance(email.getEMail().getValueSafe(), true)));

			boxPhoneNumber.getChildren().clear();
			theDetailData.getPhoneNumber().stream().forEach(phonenumber -> boxPhoneNumber.getChildren().add(CopyTextPane.createInstance(((PhoneNumberModel) phonenumber).getStandard().getValueSafe(), true)));

			boxAddress.getChildren().clear();
			theDetailData.getAddress().stream().forEach(address -> boxAddress.getChildren().add(CopyTextPane.createInstance(address.getDisplayText().getValueSafe(), true)));

			try {
				if (theDetailData.existsImageFile(LocalPrefs.get(PrefKey.PATHS_IMAGE))) {
					File fleImage = Paths.get(LocalPrefs.get(PrefKey.PATHS_IMAGE), String.format("%s.jpg", theDetailData.getFileName().getValue())).toFile();
					imgView.setImage(new Image(fleImage.toURI().toURL().toString()));
				} else {
					File fleImage = Paths.get(LocalPrefs.get(PrefKey.PATHS_IMAGE), "missing.jpg").toFile();
					if (fleImage.exists()) {
						imgView.setImage(new Image(fleImage.toURI().toURL().toString()));
					} else {
						imgView.setImage(null);
					}
				}
			} catch (Exception e) {
				RefereeManager.logger.throwing(e);
				imgView.setImage(null);
			}


		}

	}

}

/* EOF */
