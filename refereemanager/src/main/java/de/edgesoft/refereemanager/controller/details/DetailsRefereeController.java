package de.edgesoft.refereemanager.controller.details;

import de.edgesoft.edgeutils.javafx.LabeledUtils;
import de.edgesoft.refereemanager.model.RefereeModel;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

/**
 * Controller for the referee details scene.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.15.0
 */
public class DetailsRefereeController<T extends RefereeModel> extends DetailsPersonController<T> {

	/**
	 * Training level.
	 */
	@FXML
	private Label lblTrainingLevel;

	/**
	 * Club.
	 */
	@FXML
	private Label lblClub;

	/**
	 * Last update.
	 */
	@FXML
	private Label lblLastUpdate;

	/**
	 * Next update.
	 */
	@FXML
	private Label lblNextUpdate;

	/**
	 * Status.
	 */
	@FXML
	private Label lblStatus;

	/**
	 * Active?.
	 */
	@FXML
	private Label lblActive;


	/**
	 * Shows detail data.
	 *
	 * @param theDetailData (null if no data to show)
	 */
	@Override
	public void showDetails(final T theDetailData) {

		super.showDetails(theDetailData);

		if (theDetailData == null) {

			LabeledUtils.setText(lblTrainingLevel, null);
			LabeledUtils.setText(lblClub, null);
			LabeledUtils.setText(lblLastUpdate, null);
			LabeledUtils.setText(lblNextUpdate, null);
			LabeledUtils.setText(lblStatus, null);
			LabeledUtils.setText(lblActive, null);

		} else {

			lblTrainingLevel.setText(
					(theDetailData.getHighestTrainingLevel() == null) ?
							null :
							theDetailData.getHighestTrainingLevel().getType().getDisplayTitleShort().getValue());

			lblClub.setText(
					(theDetailData.getMember() == null) ?
							null :
							theDetailData.getMember().getDisplayText().getValue());

			LabeledUtils.setText(lblLastUpdate, theDetailData.getLastTrainingUpdate(), null);
			LabeledUtils.setText(lblNextUpdate, theDetailData.getNextTrainingUpdate(), "yyyy");

			lblStatus.setText(
					(theDetailData.getStatus() == null) ?
							null :
							theDetailData.getStatus().getDisplayText().getValue());

			lblActive.setText(
					(theDetailData.isActive().getValue()) ?
							"aktiv" :
							"passiv");

		}

	}

}

/* EOF */
