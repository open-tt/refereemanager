package de.edgesoft.refereemanager.controller.details;

import de.edgesoft.edgeutils.i18n.ResourceType;
import de.edgesoft.edgeutils.javafx.ControlUtils;
import de.edgesoft.edgeutils.javafx.LabeledUtils;
import de.edgesoft.refereemanager.model.TitledIDTypeModel;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

/**
 * Controller for the details part: title id.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.15.0
 */
public class DetailsTitledIDController<T extends TitledIDTypeModel> implements IDetailsController<T> {

	@FXML
	private Label lblHeading;

	/**
	 * ID label for output.
	 */
	@FXML
	private Label lblDisplayID;

	/**
	 * "ID" label.
	 */
	@FXML
	private Label lblID;

	/**
	 * Title label for output.
	 */
	@FXML
	private Label lblDisplayTitle;

	/**
	 * "Title" label.
	 */
	@FXML
	private Label lblTitle;

	/**
	 * Short title label for output.
	 */
	@FXML
	private Label lblDisplayShorttitle;

	/**
	 * "Short title" label.
	 */
	@FXML
	private Label lblShorttitle;

	/**
	 * Remark label for output.
	 */
	@FXML
	private Label lblDisplayRemark;

	/**
	 * "Remark" label.
	 */
	@FXML
	private Label lblRemark;

	/**
	 * Initializes the controller class.
	 *
	 * This method is automatically called after the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {

		// icons
		ControlUtils.fillViewControls(ResourceType.TEXT, this,
				lblHeading,
				lblID,
				lblTitle,
				lblShorttitle,
				lblRemark
				);

	}

	/**
	 * Shows detail data.
	 *
	 * @param theDetailData (null if no data to show)
	 */
	@Override
	public void showDetails(final T theDetailData) {

		if (theDetailData == null) {

			LabeledUtils.setText(lblDisplayID, null);

			LabeledUtils.setText(lblDisplayTitle, null);
			LabeledUtils.setText(lblDisplayShorttitle, null);
			LabeledUtils.setText(lblDisplayRemark, null);

		} else {

			lblDisplayID.setText(theDetailData.getId());

			LabeledUtils.setText(lblDisplayTitle, theDetailData.getTitle());
			LabeledUtils.setText(lblDisplayShorttitle, theDetailData.getShorttitle());
			LabeledUtils.setText(lblDisplayRemark, theDetailData.getRemark());

		}

	}

}

/* EOF */
