package de.edgesoft.refereemanager.controller.editdialogs;
import de.edgesoft.edgeutils.commons.ext.ModelClassExt;
import de.edgesoft.edgeutils.files.Prefs;
import de.edgesoft.edgeutils.i18n.ResourceType;
import de.edgesoft.edgeutils.javafx.ControlUtils;
import de.edgesoft.refereemanager.controller.inputforms.AbstractInputFormController;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * Abstract controller for edit dialog scenes.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.15.0
 */
public abstract class AbstractEditDialogController<T extends ModelClassExt> extends AbstractInputFormController<T> implements IEditDialogController {

	/**
	 * Reference to dialog stage.
	 */
	private Stage dialogStage = null;

	/**
	 * OK clicked?.
	 */
	private boolean okClicked = false;


	/**
	 * OK button.
	 */
	@FXML
	protected Button btnOK;

	/**
	 * Cancel button.
	 */
	@FXML
	protected Button btnCancel;



	/**
	 * Initializes the controller class.
	 *
	 * This method is automatically called after the fxml file has been loaded.
	 */
	@FXML
	protected void initialize() {

		ControlUtils.fillViewControl(btnOK, this, Prefs.SIZE_BUTTON_SMALL, ResourceType.TEXT, ResourceType.ICON);
		ControlUtils.fillViewControl(btnCancel, this, Prefs.SIZE_BUTTON_SMALL, ResourceType.TEXT, ResourceType.ICON);

	}


	@Override
	public void setDialogStage(
			final Stage theStage
			) {

        dialogStage = theStage;

    }

	@Override
	public Stage getDialogStage() {
		return dialogStage;
	}

	@Override
	public void setOkClicked(
			final boolean isOKClicked
			) {

		okClicked = isOKClicked;

	}

	@Override
	public boolean isOkClicked() {
		return okClicked;
	}

	@FXML
	@Override
    public void handleOk() {
        okClicked = true;
        dialogStage.close();
    }

	@FXML
	@Override
    public void handleCancel() {
		okClicked = false;
        dialogStage.close();
    }

}

/* EOF */
