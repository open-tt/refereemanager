package de.edgesoft.refereemanager.controller.editdialogs;
import java.util.ArrayList;
import java.util.Arrays;

import de.edgesoft.edgeutils.i18n.ResourceType;
import de.edgesoft.edgeutils.javafx.TabUtils;
import de.edgesoft.refereemanager.controller.inputforms.IInputFormController;
import de.edgesoft.refereemanager.jaxb.EventDay;
import javafx.fxml.FXML;
import javafx.scene.control.Tab;

/**
 * Controller for the referee quantity edit dialog scene.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.17.0
 */
public class EditDialogRefereeQuantityController extends AbstractTabbedEditDialogController<EventDay> {

	/**
	 * Referee quantity controller.
	 */
	@FXML
	private IInputFormController<EventDay> embeddedInputFormRefereeQuantityController;

	@FXML
	private Tab tabRefereeQuantity;

	/**
	 * Initializes the controller class.
	 *
	 * This method is automatically called after the fxml file has been loaded.
	 */
	@FXML
	@Override
	protected void initialize() {

		addInputFormController(embeddedInputFormRefereeQuantityController);

		initForm(new ArrayList<>(Arrays.asList(new Class<?>[]{EventDay.class})));

		super.initialize();

		TabUtils.fillTabs(this, ResourceType.TEXT,
				tabRefereeQuantity
				);

	}

}

/* EOF */
