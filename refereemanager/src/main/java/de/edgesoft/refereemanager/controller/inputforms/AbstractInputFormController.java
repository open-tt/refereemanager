package de.edgesoft.refereemanager.controller.inputforms;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.List;

import de.edgesoft.edgeutils.ClassUtils;
import de.edgesoft.edgeutils.commons.ext.ModelClassExt;
import de.edgesoft.refereemanager.utils.JAXBMatchUtils;

/**
 * Abstract controller for edit dialog scenes, input part.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.15.0
 */
public abstract class AbstractInputFormController<T extends ModelClassExt> implements IInputFormController<T> {

	/**
	 * Declared fields of class and abstract subclasses.
	 */
	private List<Field> lstDeclaredFields = null;

	/**
	 * Classes for introspection when setting/getting values.
	 */
	private List<Class<?>> lstClasses = null;


	@Override
	public void initForm(
			final List<Class<?>> theClasses
			) {

		lstClasses = theClasses;

		// required fields
		for (Field theFXMLField : getDeclaredFields()) {

			try {

				Object fieldObject = theFXMLField.get(this);

				JAXBMatchUtils.markRequired(theFXMLField, fieldObject, getClasses());

			} catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
				e.printStackTrace();
			}

		}

	}

	@Override
	public void fillFormFromData(
			final T theData
			) {

        // fill fields
        for (Field theFXMLField : getDeclaredFields()) {

        	try {
        		Object fieldObject = theFXMLField.get(this);

        		if (theData != null) {
        			JAXBMatchUtils.setField(theFXMLField, fieldObject, theData, getClasses());
        		} else {
        			JAXBMatchUtils.clearField(fieldObject);
        		}

        	} catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
				e.printStackTrace();
			}

        }

    }

	@Override
	public void fillDataFromForm(
			T theData
			) {

		assert (theData != null) : "data must not be null";

        for (Field theFXMLField : getDeclaredFields()) {

        	try {
        		Object fieldObject = theFXMLField.get(this);

        		JAXBMatchUtils.getField(theFXMLField, fieldObject, theData, getClasses());

        	} catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
				e.printStackTrace();
			}

        }

	}

	/**
	 * Returns declared fields.
	 *
	 * @return list of declared fields
	 */
	private List<Field> getDeclaredFields() {

		if (lstDeclaredFields == null) {
			lstDeclaredFields = ClassUtils.getDeclaredFieldsFirstAbstraction(getClass());
		}

		return lstDeclaredFields;

	}

	/**
	 * Returns introspection classes.
	 *
	 * @return list of introspection classes (empty list if none are declared)
	 */
	private List<Class<?>> getClasses() {

		if (lstClasses == null) {
			lstClasses = Collections.emptyList();
		}

		return lstClasses;

	}

}

/* EOF */
