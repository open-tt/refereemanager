package de.edgesoft.refereemanager.controller.inputforms;

import java.util.List;

import de.edgesoft.edgeutils.commons.ext.ModelClassExt;
import de.edgesoft.refereemanager.utils.JAXBMatchUtils;

/**
 * Interface for the edit dialog controller, input part.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.15.0
 */
public interface IInputFormController<T extends ModelClassExt> {

	/**
	 * Initializes form: sets introspection classes and marks required fields.
	 *
	 * The list cannot be parameterized with class T because the {@link JAXBMatchUtils} methods
	 * are static and cannot be parameterized with an according T.
	 *
	 * @param theClasses list of introspection classes
	 */
	public void initForm(
			final List<Class<?>> theClasses
			);

	/**
	 * Fills form with data to be edited.
	 *
	 * @param theData data object
	 */
	public void fillFormFromData(
			final T theData
			);

	/**
	 * Fills data object with form data.
	 *
	 * @param theData data object
	 */
	public void fillDataFromForm(
			T theData
			);

}

/* EOF */
