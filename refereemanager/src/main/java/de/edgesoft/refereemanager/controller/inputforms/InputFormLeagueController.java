package de.edgesoft.refereemanager.controller.inputforms;
import de.edgesoft.edgeutils.commons.ext.ModelClassExt;
import de.edgesoft.edgeutils.files.Prefs;
import de.edgesoft.edgeutils.i18n.ResourceType;
import de.edgesoft.edgeutils.javafx.ButtonUtils;
import de.edgesoft.edgeutils.javafx.ControlUtils;
import de.edgesoft.refereemanager.jaxb.League;
import de.edgesoft.refereemanager.model.AppModel;
import de.edgesoft.refereemanager.utils.ComboBoxUtils;
import de.edgesoft.refereemanager.utils.JAXBMatch;
import de.edgesoft.refereemanager.utils.SpinnerUtils;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;

/**
 * Controller for the league data edit dialog tab.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.15.0
 */
public class InputFormLeagueController extends AbstractInputFormController<League> {

	/**
	 * Combobox for sex types.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "sexType", jaxbclass = League.class)
	protected ComboBox<ModelClassExt> cboSexType;
	@FXML
	@JAXBMatch(jaxbfield = "sexType", jaxbclass = League.class)
	protected Label lblSexType;

	/**
	 * Clear sex types.
	 */
	@FXML
	private Button btnSexTypeClear;

	/**
	 * Spinner for rank.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "rank", jaxbclass = League.class)
	protected Spinner<Integer> spnRank;
	@FXML
	@JAXBMatch(jaxbfield = "rank", jaxbclass = League.class)
	protected Label lblRank;

	/**
	 * Checkbox for national leagues.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "national", jaxbclass = League.class)
	protected CheckBox chkNational;

	/**
	 * Results (URL).
	 */
	@FXML
	@JAXBMatch(jaxbfield = "resultsURL", jaxbclass = League.class)
	protected TextField txtResultsURL;
	@FXML
	@JAXBMatch(jaxbfield = "resultsURL", jaxbclass = League.class)
	protected Label lblResultsURL;

	/**
	 * Referee report (URL).
	 */
	@FXML
	@JAXBMatch(jaxbfield = "refereeReportURL", jaxbclass = League.class)
	protected TextField txtRefereeReportURL;
	@FXML
	@JAXBMatch(jaxbfield = "refereeReportURL", jaxbclass = League.class)
	protected Label lblRefereeReportURL;

	@FXML
	@JAXBMatch(jaxbfield = "allowanceDaily", jaxbclass = League.class)
	protected Spinner<Double> spnAllowanceDaily;
	@FXML
	@JAXBMatch(jaxbfield = "allowanceDaily", jaxbclass = League.class)
	protected Label lblAllowanceDaily;

	@FXML
	@JAXBMatch(jaxbfield = "allowancePerKM", jaxbclass = League.class)
	protected Spinner<Double> spnAllowancePerKM;
	@FXML
	@JAXBMatch(jaxbfield = "allowancePerKM", jaxbclass = League.class)
	protected Label lblAllowancePerKM;


	/**
	 * Initializes the controller class.
	 *
	 * This method is automatically called after the fxml file has been loaded.
	 */
	@FXML
	protected void initialize() {

		// fill combo boxes
        ComboBoxUtils.prepareComboBox(cboSexType, AppModel.getData().getContent().getSexType());

        // setup spinners
        SpinnerUtils.prepareIntegerSpinner(spnRank, 0, 100);
        SpinnerUtils.prepareDoubleSpinner(spnAllowanceDaily, 0.0, 100.0, 2);
        SpinnerUtils.prepareDoubleSpinner(spnAllowancePerKM, 0.0, 100.0, 2);

		// enable buttons
        ButtonUtils.bindDisable(btnSexTypeClear, cboSexType);

		// icons
		ControlUtils.fillViewControls(ResourceType.TEXT, this,
				lblSexType,
				lblRank,
				lblResultsURL,
				lblRefereeReportURL,
				chkNational,
				lblAllowanceDaily,
				lblAllowancePerKM
				);

		ControlUtils.fillViewControl(btnSexTypeClear, this, Prefs.SIZE_BUTTON_SMALL, ResourceType.ICON, ResourceType.TOOLTIP);

	}

	/**
	 * Clears sex type selection.
	 */
	@FXML
	private void handleSexTypeClear() {
		de.edgesoft.edgeutils.javafx.ComboBoxUtils.clearSelection(cboSexType);
	}

}

/* EOF */
