package de.edgesoft.refereemanager.controller.inputforms;
import de.edgesoft.refereemanager.jaxb.Address;
import de.edgesoft.refereemanager.utils.JAXBMatch;
import de.edgesoft.refereemanager.utils.LocalPrefs;
import de.edgesoft.refereemanager.utils.PrefKey;
import de.edgesoft.refereemanager.utils.SpinnerUtils;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;

/**
 * Controller for the address edit dialog scene.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.14.0
 */
public class PartInputFormAddressController extends AbstractContactInputFormController<Address> {

	/**
	 * Label for street text field.
	 *
	 * @since 0.15.0
	 */
	@FXML
	@JAXBMatch(jaxbfield = "street", jaxbclass = Address.class)
	protected Label lblStreet;

	/**
	 * Street text field.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "street", jaxbclass = Address.class)
	protected TextField txtStreet;

	/**
	 * Label for number text field.
	 *
	 * @since 0.15.0
	 */
	@FXML
	@JAXBMatch(jaxbfield = "number", jaxbclass = Address.class)
	protected Label lblNumber;

	/**
	 * Number text field.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "number", jaxbclass = Address.class)
	protected TextField txtNumber;

	/**
	 * Label for zip code text field.
	 *
	 * @since 0.15.0
	 */
	@FXML
	@JAXBMatch(jaxbfield = "zipCode", jaxbclass = Address.class)
	protected Label lblZipCode;

	/**
	 * Zip code text field.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "zipCode", jaxbclass = Address.class)
	protected TextField txtZipCode;

	/**
	 * Label for city text field.
	 *
	 * @since 0.15.0
	 */
	@FXML
	@JAXBMatch(jaxbfield = "city", jaxbclass = Address.class)
	protected Label lblCity;

	/**
	 * City text field.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "city", jaxbclass = Address.class)
	protected TextField txtCity;


	/**
	 * Spinner for latitude.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "latitude", jaxbclass = Address.class)
	protected Spinner<Double> spnLatitude;

	/**
	 * Spinner for longitude.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "longitude", jaxbclass = Address.class)
	protected Spinner<Double> spnLongitude;


	/**
	 * Initializes the controller class.
	 *
	 * This method is automatically called after the fxml file has been loaded.
	 */
	@FXML
	@Override
	protected void initialize() {

		super.initialize();

		// spinners
        SpinnerUtils.prepareDoubleSpinner(spnLatitude, -90.0, 90.0, LocalPrefs.getInt(PrefKey.GEO_COORDS_DECIMAL_PLACES));
        SpinnerUtils.prepareDoubleSpinner(spnLongitude, -180.0, 180.0, LocalPrefs.getInt(PrefKey.GEO_COORDS_DECIMAL_PLACES));

	}

}

/* EOF */
