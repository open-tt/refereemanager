package de.edgesoft.refereemanager.controller.inputforms;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import de.edgesoft.edgeutils.datetime.DateTimeUtils;
import de.edgesoft.edgeutils.files.Prefs;
import de.edgesoft.edgeutils.files.Resources;
import de.edgesoft.edgeutils.i18n.I18N;
import de.edgesoft.edgeutils.i18n.ResourceType;
import de.edgesoft.edgeutils.javafx.ControlUtils;
import de.edgesoft.edgeutils.javafx.EdgeDatePicker;
import de.edgesoft.edgeutils.javafx.SceneUtils;
import de.edgesoft.edgeutils.javafx.TimeTextField;
import de.edgesoft.refereemanager.RefereeManager;
import de.edgesoft.refereemanager.controller.editdialogs.AbstractEditDialogController;
import de.edgesoft.refereemanager.jaxb.EventDay;
import de.edgesoft.refereemanager.jaxb.RefereeAssignment;
import de.edgesoft.refereemanager.jaxb.RefereeQuantity;
import de.edgesoft.refereemanager.model.AppModel;
import de.edgesoft.refereemanager.utils.JAXBMatch;
import javafx.beans.binding.BooleanBinding;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Controller for the input form part: event day without referee assignments.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.15.0
 */
public class PartInputFormEventDayController extends AbstractInputFormController<EventDay> {

	/**
	 * Label for picker for date.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "date", jaxbclass = EventDay.class)
	protected Label lblDateLabel;

	/**
	 * Picker for date.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "date", jaxbclass = EventDay.class)
	protected EdgeDatePicker pckDate;

	/**
	 * Label for referee meeting time.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "refereeMeetingTime", jaxbclass = EventDay.class)
	protected Label lblRefereeMeetingTime;

	/**
	 * Referee meeting time.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "refereeMeetingTime", jaxbclass = EventDay.class)
	protected TimeTextField txtRefereeMeetingTime;

	/**
	 * Label for start time.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "startTime", jaxbclass = EventDay.class)
	protected Label lblStartTime;

	/**
	 * Start time.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "startTime", jaxbclass = EventDay.class)
	protected TimeTextField txtStartTime;

	/**
	 * Label for end time.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "endTime", jaxbclass = EventDay.class)
	protected Label lblEndTime;

	/**
	 * End time.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "endTime", jaxbclass = EventDay.class)
	protected TimeTextField txtEndTime;

	/**
	 * Label for referee quantity display.
	 *
	 * @since 0.17.0
	 */
	@FXML
	protected Label lblRefereeQuantitiesLabel;

	/**
	 * Referee quantity display.
	 *
	 * @since 0.17.0
	 */
	@FXML
	protected Label lblRefereeQuantities;

	/**
	 * Edit referee quantities.
	 *
	 * @since 0.17.0
	 */
	@FXML
	private Button btnEditRefereeQuantities;

	/**
	 * Label for referee assignment display.
	 *
	 * @since 0.17.0
	 */
	@FXML
	protected Label lblRefereeAssignmentsLabel;

	/**
	 * Referee assignment display.
	 *
	 * @since 0.17.0
	 */
	@FXML
	protected Label lblRefereeAssignments;

	/**
	 * Edit referee assignments.
	 *
	 * @since 0.17.0
	 */
	@FXML
	private Button btnEditRefereeAssignments;

	/**
	 * Data storage for referee quantities.
	 */
	private List<RefereeQuantity> lstRefereeQuantities = null;

	/**
	 * Data storage for referee assignments.
	 */
	private List<RefereeAssignment> lstRefereeAssignments = null;


	/**
	 * Initializes the controller class.
	 *
	 * This method is automatically called after the fxml file has been loaded.
	 */
	@FXML
	protected void initialize() {

		// set date picker date format
        pckDate.setConverter(DateTimeUtils.getDateConverter("d.M.yyyy"));

		// enable buttons
        BooleanBinding noDate = pckDate.valueProperty().isNull();
		btnEditRefereeQuantities.disableProperty().bind(
				noDate
		);
		btnEditRefereeAssignments.disableProperty().bind(
				noDate
		);

		// icons
		ControlUtils.fillViewControls(ResourceType.TEXT, this,
				lblDateLabel,
				lblRefereeMeetingTime,
				lblStartTime,
				lblEndTime,
				lblRefereeQuantitiesLabel,
				lblRefereeAssignmentsLabel
				);
		ControlUtils.fillViewControl(btnEditRefereeQuantities, this, Prefs.SIZE_BUTTON_SMALL, ResourceType.ICON, ResourceType.TOOLTIP);
		ControlUtils.fillViewControl(btnEditRefereeAssignments, this, Prefs.SIZE_BUTTON_SMALL, ResourceType.ICON, ResourceType.TOOLTIP);

		ControlUtils.fillViewControls(ResourceType.PROMPTTEXT, this,
				pckDate,
				txtRefereeMeetingTime,
				txtStartTime,
				txtEndTime
				);

		// init form
		initForm(new ArrayList<>(Arrays.asList(new Class<?>[]{EventDay.class})));

		updateRefereeQuantities();
		updateRefereeAssignments();

	}

	@Override
	public void fillFormFromData(
			final EventDay theData
			) {

		super.fillFormFromData(theData);

		if (theData == null) {
			lstRefereeQuantities = null;
			lstRefereeAssignments = null;
		} else {
			lstRefereeQuantities = new ArrayList<>(theData.getRefereeQuantity());
			lstRefereeAssignments = new ArrayList<>(theData.getRefereeAssignment());
		}

		updateRefereeQuantities();
		updateRefereeAssignments();

	}

	@Override
	public void fillDataFromForm(
			final EventDay theData
			) {

		super.fillDataFromForm(theData);

		// fill quantities
		theData.getRefereeQuantity().clear();
		if (lstRefereeQuantities != null) {
			theData.getRefereeQuantity().addAll(lstRefereeQuantities);
		}

		// fill assignments
		theData.getRefereeAssignment().clear();
		if (lstRefereeAssignments != null) {
			theData.getRefereeAssignment().addAll(lstRefereeAssignments);
		}

	}

	/**
	 * Updates referee quantity display.
	 *
	 * @since 0.17.0
	 */
	private void updateRefereeQuantities() {

		if (lstRefereeQuantities == null) {

			lblRefereeQuantities.setText("");

		} else {

			if (lstRefereeQuantities.isEmpty()) {
				lblRefereeQuantities.setText(I18N.getText("refman.basics.noentriesshort.text").get());
			} else {
				lblRefereeQuantities.setText(lstRefereeQuantities.stream().map(update -> update.getDisplayText().getValueSafe()).collect(Collectors.joining("\n")));
			}

		}

	}

	/**
	 * Updates referee assignment display.
	 *
	 * @since 0.17.0
	 */
	private void updateRefereeAssignments() {

		if (lstRefereeAssignments == null) {

			lblRefereeAssignments.setText("");

		} else {

			if (lstRefereeAssignments.isEmpty()) {
				lblRefereeAssignments.setText(I18N.getText("refman.basics.noentriesshort.text").get());
			} else {
				lblRefereeAssignments.setText(lstRefereeAssignments.stream().map(update -> update.getDisplayText().getValueSafe()).collect(Collectors.joining("\n")));
			}

		}

	}

	/**
	 * Opens referee quantity edit form.
	 *
	 * @since 0.17.0
	 */
	@FXML
	private void handleEditRefereeQuantities() {

		Map.Entry<Parent, FXMLLoader> pneLoad = Resources.loadNode("editdialogs/EditDialogRefereeQuantity");

		// Create the dialog Stage.
		Stage dialogStage = new Stage();
		dialogStage.initModality(Modality.WINDOW_MODAL);
		dialogStage.initOwner(RefereeManager.getAppLayoutController().getPrimaryStage());

		dialogStage.setScene(SceneUtils.createScene(pneLoad.getKey(), RefereeManager.CSS));

		// Set data
		AbstractEditDialogController<EventDay> editController = pneLoad.getValue().getController();
		editController.setDialogStage(dialogStage);
		dialogStage.setTitle(I18N.getViewNodeText(editController, "title", ResourceType.TEXT, pckDate.getValue()).get());

		EventDay theData = AppModel.factory.createEventDay();
		theData.getRefereeQuantity().addAll(lstRefereeQuantities);
		editController.fillFormFromData(theData);

		// Show the dialog and wait until the user closes it
		dialogStage.showAndWait();

		// ok = store data
		boolean isOkClicked = editController.isOkClicked();

		if (isOkClicked) {
			editController.fillDataFromForm(theData);
			lstRefereeQuantities.clear();
			lstRefereeQuantities = new ArrayList<>(theData.getRefereeQuantity());
		}

		updateRefereeQuantities();

	}

	/**
	 * Opens referee quantity edit form.
	 *
	 * @since 0.17.0
	 */
	@FXML
	private void handleEditRefereeAssignments() {

		Map.Entry<Parent, FXMLLoader> pneLoad = Resources.loadNode("editdialogs/EditDialogRefereeAssignment");

		// Create the dialog Stage.
		Stage dialogStage = new Stage();
		dialogStage.initModality(Modality.WINDOW_MODAL);
		dialogStage.initOwner(RefereeManager.getAppLayoutController().getPrimaryStage());

		dialogStage.setScene(SceneUtils.createScene(pneLoad.getKey(), RefereeManager.CSS));

		// Set data
		AbstractEditDialogController<EventDay> editController = pneLoad.getValue().getController();
		editController.setDialogStage(dialogStage);
		dialogStage.setTitle(I18N.getViewNodeText(editController, "title", ResourceType.TEXT, pckDate.getValue()).get());

		EventDay theData = AppModel.factory.createEventDay();
		theData.getRefereeAssignment().addAll(lstRefereeAssignments);
		editController.fillFormFromData(theData);

		// Show the dialog and wait until the user closes it
		dialogStage.showAndWait();

		// ok = store data
		boolean isOkClicked = editController.isOkClicked();

		if (isOkClicked) {
			editController.fillDataFromForm(theData);
			lstRefereeAssignments.clear();
			lstRefereeAssignments = new ArrayList<>(theData.getRefereeAssignment());
		}

		updateRefereeAssignments();

	}

}

/* EOF */
