package de.edgesoft.refereemanager.controller.inputforms;
import java.util.ArrayList;
import java.util.Arrays;

import de.edgesoft.edgeutils.commons.ext.ModelClassExt;
import de.edgesoft.edgeutils.files.Prefs;
import de.edgesoft.edgeutils.files.Resources;
import de.edgesoft.refereemanager.jaxb.RefereeQuantity;
import de.edgesoft.refereemanager.model.AppModel;
import de.edgesoft.refereemanager.utils.ComboBoxUtils;
import de.edgesoft.refereemanager.utils.JAXBMatch;
import de.edgesoft.refereemanager.utils.SpinnerUtils;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.image.ImageView;

/**
 * Controller for the referee quantity data edit dialog tab.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.17.0
 */
public class PartInputFormRefereeQuantityController extends AbstractInputFormController<RefereeQuantity> {

	/**
	 * Label for combobox for referee assignment type.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "refereeAssignmentType", jaxbclass = RefereeQuantity.class)
	protected Label lblRefereeAssignmentType;

	/**
	 * Combobox for referee assignment type.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "refereeAssignmentType", jaxbclass = RefereeQuantity.class)
	protected ComboBox<ModelClassExt> cboRefereeAssignmentType;

	/**
	 * Clear referee assignment type.
	 */
	@FXML
	private Button btnRefereeAssignmentTypeClear;

	/**
	 * Label for spinner for quantity.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "quantity", jaxbclass = RefereeQuantity.class)
	protected Label lblQuantity;

	/**
	 * Spinner for quantity.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "quantity", jaxbclass = RefereeQuantity.class)
	protected Spinner<Integer> spnQuantity;


	/**
	 * Initializes the controller class.
	 *
	 * This method is automatically called after the fxml file has been loaded.
	 */
	@FXML
	protected void initialize() {

		// setup spinners
		SpinnerUtils.prepareIntegerSpinner(spnQuantity, 0, 50);

		// fill combo boxes
        ComboBoxUtils.prepareComboBox(cboRefereeAssignmentType, AppModel.getData().getContent().getRefereeAssignmentType());

		// enable buttons
		btnRefereeAssignmentTypeClear.disableProperty().bind(
				cboRefereeAssignmentType.getSelectionModel().selectedItemProperty().isNull()
		);

		// icons
		btnRefereeAssignmentTypeClear.setGraphic(new ImageView(Resources.loadImage("icons/actions/edit-clear.svg", Prefs.SIZE_BUTTON_SMALL)));

		// init form
		initForm(new ArrayList<>(Arrays.asList(new Class<?>[]{RefereeQuantity.class})));

	}

	/**
	 * Clears referee assignment type selection.
	 */
	@FXML
	private void handleRefereeAssignmentTypeClear() {
		de.edgesoft.edgeutils.javafx.ComboBoxUtils.clearSelection(cboRefereeAssignmentType);
	}

}

/* EOF */
