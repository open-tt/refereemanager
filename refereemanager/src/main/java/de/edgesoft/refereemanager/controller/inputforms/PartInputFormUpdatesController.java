package de.edgesoft.refereemanager.controller.inputforms;
import java.util.ArrayList;
import java.util.Arrays;

import de.edgesoft.edgeutils.datetime.DateTimeUtils;
import de.edgesoft.edgeutils.javafx.EdgeDatePicker;
import de.edgesoft.refereemanager.jaxb.Update;
import de.edgesoft.refereemanager.utils.JAXBMatch;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

/**
 * Controller for the input form part: update.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.15.0
 */
public class PartInputFormUpdatesController extends AbstractInputFormController<Update> {

	/**
	 * Label for date picker.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "date", jaxbclass = Update.class)
	protected Label lblDate;

	/**
	 * Date picker.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "date", jaxbclass = Update.class)
	protected EdgeDatePicker pckDate;


	/**
	 * Initializes the controller class.
	 *
	 * This method is automatically called after the fxml file has been loaded.
	 */
	@FXML
	protected void initialize() {

		// set date picker date format
		pckDate.setConverter(DateTimeUtils.getDateConverter("d.M.yyyy"));

		// init form
		initForm(new ArrayList<>(Arrays.asList(new Class<?>[]{Update.class})));

	}

}

/* EOF */
