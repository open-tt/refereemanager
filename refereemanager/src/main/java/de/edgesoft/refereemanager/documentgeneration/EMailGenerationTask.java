package de.edgesoft.refereemanager.documentgeneration;

import java.io.FileNotFoundException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.PasswordAuthentication;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import de.edgesoft.edgeutils.datetime.DateTimeUtils;
import de.edgesoft.edgeutils.i18n.ResourceType;
import de.edgesoft.edgeutils.javafx.ControlUtils;
import de.edgesoft.refereemanager.RefereeManager;
import de.edgesoft.refereemanager.controller.datatables.AbstractDataTableController;
import de.edgesoft.refereemanager.documentgenerationqueue.jaxb.DocumentGenerationJob;
import de.edgesoft.refereemanager.jaxb.EMail;
import de.edgesoft.refereemanager.messagetext.jaxb.Attachment;
import de.edgesoft.refereemanager.messagetext.jaxb.MessageTextElement;
import de.edgesoft.refereemanager.model.PersonModel;
import de.edgesoft.refereemanager.model.TitledIDTypeModel;
import de.edgesoft.refereemanager.utils.DocumentGenerationData;
import de.edgesoft.refereemanager.utils.DocumentGenerationResult;
import de.edgesoft.refereemanager.utils.LocalPrefs;
import de.edgesoft.refereemanager.utils.MailUtils;
import de.edgesoft.refereemanager.utils.PrefKey;
import de.edgesoft.refereemanager.utils.RefManException;
import freemarker.template.Template;
import javafx.concurrent.Task;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

/**
 * Email generation task.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.17.0
 */
public class EMailGenerationTask extends AbstractDocumentGenerationTask implements IDocumentGenerationTask {

	/**
	 * Test sending or send?
	 */
	private final String TEST_MAIL = "chkTestMail";

	/**
	 * CC to sender?
	 */
	private final String EMAIL_CC_SENDER = "chkEMailCCSender";

	/**
	 * CC to recipients?
	 */
	private final String EMAIL_CC_REPLY = "chkEMailCCReply";


	@Override
	public Optional<Set<DataFeature>> getRequiredDataFeatures() {
		return Optional.of(Set.of(DataFeature.HAS_EMAIL));
	}

	@Override
	public Set<MessageTextElement> getMandatoryDocumentData() {
		return Set.of(
				MessageTextElement.BODY,
				MessageTextElement.CLOSING,
				MessageTextElement.OPENING,
				MessageTextElement.SIGNATURE,
				MessageTextElement.TITLE
				);
	}

	@Override
	public Set<MessageTextElement> getOptionalDocumentData() {
		return Set.of(
				MessageTextElement.ATTACHMENT,
				MessageTextElement.DATE,
				MessageTextElement.SUBTITLE
				);
	}

	@Override
	public Optional<VBox> getAdditionalInputElements() {

		VBox boxReturn = getVBoxWithHeadingAndGridPane();

		GridPane pneGrid = (GridPane) boxReturn.getChildren().stream().filter(it -> it.getId().equals(GRIDPANE_ID)).findFirst().get();

		CheckBox chkTemp = new CheckBox();
		chkTemp.setId(TEST_MAIL);
		ControlUtils.fillViewControl(chkTemp, this, ResourceType.TEXT);
		chkTemp.setSelected(true);

		pneGrid.add(chkTemp, 0, pneGrid.getRowCount(), 3, 1);

		chkTemp = new CheckBox();
		chkTemp.setId(EMAIL_CC_SENDER);
		ControlUtils.fillViewControl(chkTemp, this, ResourceType.TEXT);
		// TODO not functional
		chkTemp.setDisable(true);

		pneGrid.add(chkTemp, 0, pneGrid.getRowCount(), 3, 1);

		chkTemp = new CheckBox();
		chkTemp.setId(EMAIL_CC_REPLY);
		ControlUtils.fillViewControl(chkTemp, this, ResourceType.TEXT);
		// TODO not functional
		chkTemp.setDisable(true);

		pneGrid.add(chkTemp, 0, pneGrid.getRowCount(), 3, 1);

		return Optional.of(boxReturn);

	}

	/**
	 * Returns email sending task.
	 *
	 * Every mail is sent individually for two reasons:
	 *
	 * 1. If there are multiple recipients, {@link Transport#send(Message, String, String)} sends
	 * all mails in a transaction, meaning if one fails, all fail.
	 * This is bad, because mails frequently fail, meaning one has to resend all Mails.
	 *
	 * 2. I have to use individualized mails (per template), this only works
	 * with individual mails.
	 *
	 * @param theGenerationJob document generation job
	 * @param theRecipientList data table controller with selection
	 * @return document generation task
	 */
	@Override
	public Task<Map<DocumentGenerationResult, Integer>> getDocumentGenerationTask(
			final DocumentGenerationJob theGenerationJob,
			final AbstractDataTableController<TitledIDTypeModel> theRecipientList
			) {

		Objects.requireNonNull(theGenerationJob, "document generation job must not be null");

		return new Task<>() {

			@Override
			protected Map<DocumentGenerationResult, Integer> call() throws InterruptedException {

				int iSuccess = 0;
				int iError = 0;

				try {

					// load email template
					updateMessage("Lade Mail-Template.");
					Template tplEMail = DocumentGenerationUtils.loadTemplate(LocalPrefs.get(PrefKey.EMAIL_TEMPLATE_EMAIL));

					// prepare email
					Properties mailProps = new Properties();
					mailProps.setProperty("mail.smtp.host", LocalPrefs.get(PrefKey.EMAIL_SMTP_HOST));
					mailProps.setProperty("mail.smtp.auth", "true");

					RefereeManager.logger.info(MessageFormat.format("Generiere Mail-Session zu ''{0}''.", mailProps.getProperty("mail.smtp.host")));
					Session session = Session.getInstance(mailProps, new Authenticator() {
						@Override
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(LocalPrefs.get(PrefKey.EMAIL_SMTP_USERNAME), LocalPrefs.get(PrefKey.EMAIL_SMTP_PASSWORD));
						}
					});

					// get selection, change to people, filter for emails, never send to dead people
					RefereeManager.logger.info("Hole die ausgewählten Mailempfänger.");
					List<PersonModel> lstSelection = theRecipientList.getSelectedItemsAsDisplayed().stream()
							.filter(it -> it instanceof PersonModel)
							.map(PersonModel.class::cast)
							.filter(PersonModel.HAS_EMAIL)
							.filter(PersonModel.IS_ALIVE)
							.collect(Collectors.toCollection(ArrayList::new))
							;

					if (lstSelection.isEmpty()) {
						throw new RefManException("Auswahl ist leer.");
					}

					// send email for every person individually (see remark in method doc)
					final int iCount = lstSelection.size();
					for (PersonModel person : lstSelection) {

						// pause after some emails if neccessary
						if ((LocalPrefs.getInt(PrefKey.EMAIL_PAUSE_FREQUENCY) > 0) && (LocalPrefs.getInt(PrefKey.EMAIL_PAUSE_DURATION) > 0)) {

							if (((iSuccess + iError) > 0) && ((iSuccess + iError) % LocalPrefs.getInt(PrefKey.EMAIL_PAUSE_FREQUENCY) == 0)) {
								updateMessage(MessageFormat.format("Pause von {0,choice,1#einer Sekunde|1<{0,number,integer} Sekunden}.", LocalPrefs.getInt(PrefKey.EMAIL_PAUSE_DURATION)));
								TimeUnit.SECONDS.sleep(LocalPrefs.getInt(PrefKey.EMAIL_PAUSE_DURATION));
							}

						}

						RefereeManager.logger.info(MessageFormat.format("Mail an ''{0}''.", person.getDisplayTitle().getValue()));
						updateMessage(MessageFormat.format("Mail an ''{0}''.", person.getDisplayTitle().getValue()));

						// fill variables in generated content
						DocumentGenerationData dtaDocument = DocumentGenerationUtils.fillDocumentGenerationData(theGenerationJob.getMessageContent(), lstSelection, person);
						RefereeManager.logger.info(MessageFormat.format("Subject: ''{0}''.", dtaDocument.getFilledInputData().getValueFor(MessageTextElement.TITLE)));

						try {

							Message msgMail = new MimeMessage(session);

							// from
							msgMail.setFrom(new InternetAddress(LocalPrefs.get(PrefKey.EMAIL_FROM_EMAIL), LocalPrefs.get(PrefKey.EMAIL_FROM_NAME), StandardCharsets.UTF_8.name()));

							// reply to
							List<InternetAddress> lstReplyTo = new ArrayList<>();
							lstReplyTo.add(new InternetAddress(LocalPrefs.get(PrefKey.EMAIL_FROM_EMAIL), LocalPrefs.get(PrefKey.EMAIL_FROM_NAME), StandardCharsets.UTF_8.name()));
							if (!LocalPrefs.get(PrefKey.EMAIL_REPLY_TO_EMAIL1).isEmpty()) {
								lstReplyTo.add(new InternetAddress(LocalPrefs.get(PrefKey.EMAIL_REPLY_TO_EMAIL1), LocalPrefs.get(PrefKey.EMAIL_REPLY_TO_NAME1), StandardCharsets.UTF_8.name()));
							}
							if (!LocalPrefs.get(PrefKey.EMAIL_REPLY_TO_EMAIL2).isEmpty()) {
								lstReplyTo.add(new InternetAddress(LocalPrefs.get(PrefKey.EMAIL_REPLY_TO_EMAIL2), LocalPrefs.get(PrefKey.EMAIL_REPLY_TO_NAME2), StandardCharsets.UTF_8.name()));
							}
							msgMail.setReplyTo(lstReplyTo.toArray(new InternetAddress[lstReplyTo.size()]));

							// date
							msgMail.setSentDate(DateTimeUtils.toDate(DateTimeUtils.parseDateTime(dtaDocument.getFilledInputData().getValueFor(MessageTextElement.DATE), DateTimeFormatter.ISO_LOCAL_DATE_TIME)));

							// subject
							msgMail.setSubject(dtaDocument.getFilledInputData().getValueFor(MessageTextElement.TITLE));

							// recipient
							EMail theEMail = person.getPrimaryEMail();
							msgMail.setRecipient(RecipientType.TO, new InternetAddress(theEMail.getEMail().getValue(), person.getFullName().getValue(), StandardCharsets.UTF_8.name()));

							// content
							MimeMultipart msgContent = new MimeMultipart();
							MimeBodyPart text = new MimeBodyPart();
							text.setText(DocumentGenerationUtils.processTemplate(tplEMail, dtaDocument));
							msgContent.addBodyPart(text);
							msgMail.setContent(msgContent);

							// attachments (no .foreach because of exception handling)
							for (Attachment theAttachment : dtaDocument.getFilledInputData().getAttachment()) {

								Path attPath = Paths.get(theAttachment.getFilename().getValue());

								if (!attPath.toFile().exists()) {
									throw new FileNotFoundException(attPath.toAbsolutePath().normalize().toString());
								}

								BodyPart attBodyPart = new MimeBodyPart();
								attBodyPart.setDataHandler(new DataHandler(new FileDataSource(attPath.toFile())));
								attBodyPart.setFileName(attPath.getFileName().toString());

								msgContent.addBodyPart(attBodyPart);

								RefereeManager.logger.info(MessageFormat.format("Attachment: {0}", attBodyPart.getFileName()));

							}

							if (Boolean.parseBoolean(theGenerationJob.getTaskInput().get(TEST_MAIL).getValue())) {
								RefereeManager.logger.info(MailUtils.toString(msgMail));
								RefereeManager.logger.info("Test! Nicht gesendet.");
							} else {
								Transport.send(msgMail);
								RefereeManager.logger.info("gesendet.");
							}
							iSuccess++;

						} catch (SendFailedException e) {
							if (e.getInvalidAddresses() != null) {
								Arrays.asList(e.getInvalidAddresses()).forEach(adr ->
										RefereeManager.logger.error(MessageFormat.format("nicht gesendet, fehlerhafte Adresse: {0} ({1})", adr.toString(), e.getMessage())));
							}
							if (e.getValidUnsentAddresses() != null) {
								Arrays.asList(e.getValidUnsentAddresses()).forEach(adr ->
										RefereeManager.logger.error(MessageFormat.format("nicht gesendet, valide Adresse: {0} ({1})", adr.toString(), e.getMessage())));
							}
							if (e.getValidSentAddresses() != null) {
								Arrays.asList(e.getValidSentAddresses()).forEach(adr ->
										RefereeManager.logger.error(MessageFormat.format("gesendet, valide Adresse: {0} ({1})", adr.toString(), e.getMessage())));
							}
							iError++;
						}

						updateProgress(iError + iSuccess, iCount);

					}

				} catch (Exception e) {
					RefereeManager.logger.error(e);
					e.printStackTrace();
					iError++;
				}

				return Map.of(DocumentGenerationResult.SUCCESS, iSuccess, DocumentGenerationResult.ERROR, iError);

			}

		};

	}

}

/* EOF */
