package de.edgesoft.refereemanager.documentgeneration;

import java.io.IOException;
import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.util.Strings;

import de.edgesoft.refereemanager.RefereeManager;
import de.edgesoft.refereemanager.controller.datatables.AbstractDataTableController;
import de.edgesoft.refereemanager.documentgenerationqueue.jaxb.DocumentGenerationJob;
import de.edgesoft.refereemanager.messagetext.jaxb.MessageTextElement;
import de.edgesoft.refereemanager.model.PersonModel;
import de.edgesoft.refereemanager.model.TitledIDTypeModel;
import de.edgesoft.refereemanager.utils.DocumentGenerationData;
import de.edgesoft.refereemanager.utils.DocumentGenerationResult;
import de.edgesoft.refereemanager.utils.LocalPrefs;
import de.edgesoft.refereemanager.utils.PrefKey;
import de.edgesoft.refereemanager.utils.RefManException;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import javafx.concurrent.Task;

/**
 * Letter generation task.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.17.0
 */
public class LetterGenerationTask extends AbstractDocumentGenerationTask implements IDocumentGenerationTask {

	@Override
	public Optional<Set<DataFeature>> getRequiredDataFeatures() {
		return Optional.of(Set.of(DataFeature.HAS_ADDRESS));
	}

	@Override
	public Set<MessageTextElement> getMandatoryDocumentData() {
		return Set.of(
				MessageTextElement.BODY,
				MessageTextElement.CLOSING,
				MessageTextElement.FILENAME,
				MessageTextElement.OPENING,
				MessageTextElement.SIGNATURE,
				MessageTextElement.TITLE
				);
	}

	@Override
	public Set<MessageTextElement> getOptionalDocumentData() {
		return Set.of(
				MessageTextElement.ATTACHMENT,
				MessageTextElement.DATE,
				MessageTextElement.OPTIONS,
				MessageTextElement.OUTPUTPATH,
				MessageTextElement.SUBTITLE
				);
	}

	/**
	 * Returns letter generation task.
	 *
	 * @param theGenerationJob document generation job
	 * @param theRecipientList data table controller with selection
	 * @return document generation task
	 */
	@Override
	public Task<Map<DocumentGenerationResult, Integer>> getDocumentGenerationTask(
			final DocumentGenerationJob theGenerationJob,
			final AbstractDataTableController<TitledIDTypeModel> theRecipientList
			) {

		Objects.requireNonNull(theGenerationJob, "document generation job must not be null");

		return new Task<>() {

			/** Main execution method. */
			@Override
			protected Map<DocumentGenerationResult, Integer> call() throws InterruptedException {

				int iSuccess = 0;
				int iError = 0;

				try {

					// load document template
					updateMessage("Lade Brief-Template.");
					Template tplLetter = DocumentGenerationUtils.loadTemplate(LocalPrefs.get(PrefKey.LETTERS_TEMPLATE_LETTER));

					// get selection, change to people, filter for address, never send to dead people
					RefereeManager.logger.info("Hole die ausgewählten Briefempfänger.");
					List<PersonModel> lstSelection = theRecipientList.getSelectedItemsAsDisplayed().stream()
							.filter(it -> it instanceof PersonModel)
							.map(PersonModel.class::cast)
							.filter(PersonModel.HAS_ADDRESS)
							.filter(PersonModel.IS_ALIVE)
							.collect(Collectors.toCollection(ArrayList::new))
							;

					if (lstSelection.isEmpty()) {
						throw new RefManException("Auswahl ist leer.");
					}

					// load single merge template
					Template tplMergeSingle = null;
					String sTemplateName = LocalPrefs.get(PrefKey.LETTERS_TEMPLATE_MERGE_SINGLE);
					if (!Strings.isBlank(sTemplateName)) {

						updateMessage("Lade Single-Merge-Template.");
						tplMergeSingle = DocumentGenerationUtils.loadTemplate(sTemplateName);

					}

					// load all merge template
					Template tplMergeAll = null;
					sTemplateName = LocalPrefs.get(PrefKey.LETTERS_TEMPLATE_MERGE_ALL);
					if (!Strings.isBlank(sTemplateName)) {

						updateMessage("Lade All-Merge-Template.");
						tplMergeAll = DocumentGenerationUtils.loadTemplate(sTemplateName);

					}

					List<String> lstFilenames = new ArrayList<>();
					Path pathOutput = null;

					int iCount = lstSelection.size() + ((tplMergeSingle == null) ? 0 : lstSelection.size()) + ((tplMergeAll == null) ? 0 : 1);
					for (PersonModel person : lstSelection) {

						RefereeManager.logger.info(MessageFormat.format("Brief an ''{0}''.", person.getDisplayTitle().getValue()));
						updateMessage(MessageFormat.format("Brief an ''{0}''.", person.getDisplayTitle().getValue()));

						// fill variables in generated content
						DocumentGenerationData dtaDocument = DocumentGenerationUtils.fillDocumentGenerationData(theGenerationJob.getMessageContent(), lstSelection, person);
						RefereeManager.logger.info(MessageFormat.format("Titel: ''{0}''.", dtaDocument.getFilledInputData().getValueFor(MessageTextElement.TITLE)));

						try {

							pathOutput = DocumentGenerationUtils.getFilledOutputPath(
									dtaDocument.getFilledInputData().getValueFor(MessageTextElement.OUTPUTPATH),
									dtaDocument.getFilledInputData().getValueFor(MessageTextElement.FILENAME),
									dtaDocument
									);
							DocumentGenerationUtils.processAndSaveTemplate(tplLetter, dtaDocument, pathOutput);
							iSuccess++;

							// fill single merge template, write document
							if (tplMergeSingle != null) {

								String sFilename = pathOutput.getFileName().toString()
										.replace(".md", ".tex")
										.replace(".mmd", ".tex");
								DocumentGenerationUtils.processAndSaveTemplate(tplMergeSingle, dtaDocument, Path.of(pathOutput.getParent().toString(), sFilename));
								lstFilenames.add(sFilename.replace(".tex", ".pdf"));

								iSuccess++;

							}

						} catch (IOException | TemplateException e) {
							RefereeManager.logger.error(e);
							e.printStackTrace();
							iError++;
						}

						updateProgress(iError + iSuccess, iCount);

					}

					// fill all merge template, write document
					if (tplMergeAll != null) {

						DocumentGenerationData dtaDocument = DocumentGenerationUtils.createDocumentGenerationData(lstSelection, null);
						dtaDocument.setFilenames(lstFilenames);

						DocumentGenerationUtils.processAndSaveTemplate(tplMergeAll, dtaDocument, Path.of(pathOutput.getParent().toString(), "merge_all.tex"));
						iSuccess++;

					}

				} catch (Exception e) {
					RefereeManager.logger.error(e);
					e.printStackTrace();
					iError++;
				}

				return Map.of(DocumentGenerationResult.SUCCESS, iSuccess, DocumentGenerationResult.ERROR, iError);

			}

		};

	}

}

/* EOF */
