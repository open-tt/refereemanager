package de.edgesoft.refereemanager.documentgeneration;

import java.io.IOException;
import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import de.edgesoft.refereemanager.RefereeManager;
import de.edgesoft.refereemanager.controller.datatables.AbstractDataTableController;
import de.edgesoft.refereemanager.documentgenerationqueue.jaxb.DocumentGenerationJob;
import de.edgesoft.refereemanager.messagetext.jaxb.MessageTextElement;
import de.edgesoft.refereemanager.model.TitledIDTypeModel;
import de.edgesoft.refereemanager.utils.DocumentGenerationData;
import de.edgesoft.refereemanager.utils.DocumentGenerationResult;
import de.edgesoft.refereemanager.utils.LocalPrefs;
import de.edgesoft.refereemanager.utils.PrefKey;
import de.edgesoft.refereemanager.utils.RefManException;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import javafx.concurrent.Task;

/**
 * Texts generation task.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.17.0
 */
public class MultipleTextsGenerationTask extends AbstractDocumentGenerationTask implements IDocumentGenerationTask {

	@Override
	public Set<MessageTextElement> getMandatoryDocumentData() {
		return Set.of(
				MessageTextElement.BODY,
				MessageTextElement.FILENAME
				);
	}

	@Override
	public Set<MessageTextElement> getOptionalDocumentData() {
		return Set.of(
				MessageTextElement.OUTPUTPATH
				);
	}

	/**
	 * Returns texts generation task.
	 *
	 * @param theGenerationJob document generation job
	 * @param theRecipientList data table controller with selection
	 * @return document generation task
	 */
	@Override
	public Task<Map<DocumentGenerationResult, Integer>> getDocumentGenerationTask(
			final DocumentGenerationJob theGenerationJob,
			final AbstractDataTableController<TitledIDTypeModel> theRecipientList
			) {

		Objects.requireNonNull(theGenerationJob, "document generation job must not be null");

		return new Task<>() {

			/** Main execution method. */
			@Override
			protected Map<DocumentGenerationResult, Integer> call() throws InterruptedException {

				int iSuccess = 0;
				int iError = 0;

				try {

					updateMessage("Lade Text-Template.");
					Template tplText = DocumentGenerationUtils.loadTemplate(LocalPrefs.get(PrefKey.TEXTS_TEMPLATE_TEXT));

					RefereeManager.logger.info("Hole die Auswahl.");
					List<TitledIDTypeModel> lstSelection = theRecipientList.getSelectedItemsAsDisplayed();

					if (lstSelection.isEmpty()) {
						throw new RefManException("Auswahl ist leer.");
					}

					int iCount = lstSelection.size();
					for (TitledIDTypeModel titledElement : lstSelection) {


						RefereeManager.logger.info(MessageFormat.format("Text ''{0}''.", titledElement.getDisplayTitle().getValue()));
						updateMessage(MessageFormat.format("Text ''{0}''.", titledElement.getDisplayTitle().getValue()));

						DocumentGenerationData dtaDocument = DocumentGenerationUtils.fillDocumentGenerationData(theGenerationJob.getMessageContent(), lstSelection, titledElement);

						try {

							Path pathOutput = DocumentGenerationUtils.getFilledOutputPath(
									dtaDocument.getFilledInputData().getValueFor(MessageTextElement.OUTPUTPATH),
									dtaDocument.getFilledInputData().getValueFor(MessageTextElement.FILENAME),
									dtaDocument
									);
							DocumentGenerationUtils.processAndSaveTemplate(tplText, dtaDocument, pathOutput);
							iSuccess++;

						} catch (IOException | TemplateException e) {
							RefereeManager.logger.error(e);
							iError++;
						}

						updateProgress(iError + iSuccess, iCount);

					}

				} catch (Exception e) {
					RefereeManager.logger.error(e);
					e.printStackTrace();
					iError++;
				}

				return Map.of(DocumentGenerationResult.SUCCESS, iSuccess, DocumentGenerationResult.ERROR, iError);

			}

		};

	}

}

/* EOF */
