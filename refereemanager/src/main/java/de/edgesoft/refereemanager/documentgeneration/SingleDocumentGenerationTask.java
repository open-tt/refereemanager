package de.edgesoft.refereemanager.documentgeneration;

import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import de.edgesoft.refereemanager.RefereeManager;
import de.edgesoft.refereemanager.controller.datatables.AbstractDataTableController;
import de.edgesoft.refereemanager.documentgenerationqueue.jaxb.DocumentGenerationJob;
import de.edgesoft.refereemanager.messagetext.jaxb.MessageTextElement;
import de.edgesoft.refereemanager.model.TitledIDTypeModel;
import de.edgesoft.refereemanager.utils.DocumentGenerationData;
import de.edgesoft.refereemanager.utils.DocumentGenerationResult;
import de.edgesoft.refereemanager.utils.LocalPrefs;
import de.edgesoft.refereemanager.utils.PrefKey;
import freemarker.template.Template;
import javafx.concurrent.Task;

/**
 * Document generation task.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.17.0
 */
public class SingleDocumentGenerationTask extends AbstractDocumentGenerationTask implements IDocumentGenerationTask {

	@Override
	public Set<MessageTextElement> getMandatoryDocumentData() {
		return Set.of(
				MessageTextElement.BODY,
				MessageTextElement.FILENAME,
				MessageTextElement.TITLE
				);
	}

	@Override
	public Set<MessageTextElement> getOptionalDocumentData() {
		return Set.of(
				MessageTextElement.DATE,
				MessageTextElement.OPTIONS,
				MessageTextElement.OUTPUTPATH,
				MessageTextElement.SUBTITLE
				);
	}

	/**
	 * Returns document generation task.
	 *
	 * @param theGenerationJob document generation job
	 * @param theRecipientList data table controller with selection
	 * @return document generation task
	 */
	@Override
	public Task<Map<DocumentGenerationResult, Integer>> getDocumentGenerationTask(
			final DocumentGenerationJob theGenerationJob,
			final AbstractDataTableController<TitledIDTypeModel> theRecipientList
			) {

		Objects.requireNonNull(theGenerationJob, "document generation job must not be null");

		return new Task<>() {

			/** Main execution method. */
			@Override
			protected Map<DocumentGenerationResult, Integer> call() throws InterruptedException {

				int iSuccess = 0;
				int iError = 0;

				try {

					updateMessage("Lade Dokument-Template.");
					Template tplDocument = DocumentGenerationUtils.loadTemplate(LocalPrefs.get(PrefKey.DOCUMENTS_TEMPLATE_DOCUMENT));

					RefereeManager.logger.info("Hole die Auswahl.");
					List<TitledIDTypeModel> lstSelection = theRecipientList.getSelectedItemsAsDisplayed();

					if (lstSelection.isEmpty()) {
						RefereeManager.logger.info("Die Auswahl ist leer.");
					}

					updateMessage("Erzeuge Dokument.");
					DocumentGenerationData dtaDocument = DocumentGenerationUtils.fillDocumentGenerationData(theGenerationJob.getMessageContent(), lstSelection, null);

					RefereeManager.logger.info(MessageFormat.format("Titel: ''{0}''.", dtaDocument.getFilledInputData().getValueFor(MessageTextElement.TITLE)));
					updateMessage(MessageFormat.format("Titel: ''{0}''.", dtaDocument.getFilledInputData().getValueFor(MessageTextElement.TITLE)));


					Path pathOutput = DocumentGenerationUtils.getFilledOutputPath(
							dtaDocument.getFilledInputData().getValueFor(MessageTextElement.OUTPUTPATH),
							dtaDocument.getFilledInputData().getValueFor(MessageTextElement.FILENAME),
							dtaDocument
							);
					DocumentGenerationUtils.processAndSaveTemplate(tplDocument, dtaDocument, pathOutput);
					iSuccess++;

				} catch (Exception e) {
					RefereeManager.logger.error(e);
					e.printStackTrace();
					iError++;
				}

				return Map.of(DocumentGenerationResult.SUCCESS, iSuccess, DocumentGenerationResult.ERROR, iError);

			}

		};

	}

}

/* EOF */
