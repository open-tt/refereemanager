
package de.edgesoft.refereemanager.documentgenerationqueue.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import de.edgesoft.edgeutils.commons.AbstractModelClass;
import de.edgesoft.edgeutils.javafx.SimpleStringPropertyAdapter;
import de.edgesoft.refereemanager.documentgenerationqueue.model.TaskInput;


/**
 * <p>Java class for DocumentGenerationJob complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DocumentGenerationJob"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{}AbstractModelClass"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="task_i_d" type="{}StringProperty"/&gt;
 *         &lt;element name="message_filename" type="{}StringProperty"/&gt;
 *         &lt;element name="message_content" type="{}StringProperty"/&gt;
 *         &lt;element name="message_file_options" type="{}StringProperty" minOccurs="0"/&gt;
 *         &lt;element name="message_file_output_path" type="{}StringProperty" minOccurs="0"/&gt;
 *         &lt;element name="task_input" type="{}TaskInputModeler" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentGenerationJob", propOrder = {
    "taskID",
    "messageFilename",
    "messageContent",
    "messageFileOptions",
    "messageFileOutputPath",
    "taskInput"
})
public class DocumentGenerationJob
    extends AbstractModelClass
{

    @XmlElement(name = "task_i_d", required = true, type = java.lang.String.class)
    @XmlJavaTypeAdapter(SimpleStringPropertyAdapter.class)
    protected javafx.beans.property.SimpleStringProperty taskID;
    @XmlElement(name = "message_filename", required = true, type = java.lang.String.class)
    @XmlJavaTypeAdapter(SimpleStringPropertyAdapter.class)
    protected javafx.beans.property.SimpleStringProperty messageFilename;
    @XmlElement(name = "message_content", required = true, type = java.lang.String.class)
    @XmlJavaTypeAdapter(SimpleStringPropertyAdapter.class)
    protected javafx.beans.property.SimpleStringProperty messageContent;
    @XmlElement(name = "message_file_options", type = java.lang.String.class)
    @XmlJavaTypeAdapter(SimpleStringPropertyAdapter.class)
    protected javafx.beans.property.SimpleStringProperty messageFileOptions;
    @XmlElement(name = "message_file_output_path", type = java.lang.String.class)
    @XmlJavaTypeAdapter(SimpleStringPropertyAdapter.class)
    protected javafx.beans.property.SimpleStringProperty messageFileOutputPath;
    @XmlElement(name = "task_input", type = TaskInputModeler.class)
    protected TaskInput<String, javafx.beans.property.SimpleStringProperty> taskInput;

    /**
     * Gets the value of the taskID property.
     * 
     * @return
     *     possible object is
     *     {@link java.lang.String }
     *     
     */
    public javafx.beans.property.SimpleStringProperty getTaskID() {
        return taskID;
    }

    /**
     * Sets the value of the taskID property.
     * 
     * @param value
     *     allowed object is
     *     {@link java.lang.String }
     *     
     */
    public void setTaskID(javafx.beans.property.SimpleStringProperty value) {
        this.taskID = value;
    }

    /**
     * Gets the value of the messageFilename property.
     * 
     * @return
     *     possible object is
     *     {@link java.lang.String }
     *     
     */
    public javafx.beans.property.SimpleStringProperty getMessageFilename() {
        return messageFilename;
    }

    /**
     * Sets the value of the messageFilename property.
     * 
     * @param value
     *     allowed object is
     *     {@link java.lang.String }
     *     
     */
    public void setMessageFilename(javafx.beans.property.SimpleStringProperty value) {
        this.messageFilename = value;
    }

    /**
     * Gets the value of the messageContent property.
     * 
     * @return
     *     possible object is
     *     {@link java.lang.String }
     *     
     */
    public javafx.beans.property.SimpleStringProperty getMessageContent() {
        return messageContent;
    }

    /**
     * Sets the value of the messageContent property.
     * 
     * @param value
     *     allowed object is
     *     {@link java.lang.String }
     *     
     */
    public void setMessageContent(javafx.beans.property.SimpleStringProperty value) {
        this.messageContent = value;
    }

    /**
     * Gets the value of the messageFileOptions property.
     * 
     * @return
     *     possible object is
     *     {@link java.lang.String }
     *     
     */
    public javafx.beans.property.SimpleStringProperty getMessageFileOptions() {
        return messageFileOptions;
    }

    /**
     * Sets the value of the messageFileOptions property.
     * 
     * @param value
     *     allowed object is
     *     {@link java.lang.String }
     *     
     */
    public void setMessageFileOptions(javafx.beans.property.SimpleStringProperty value) {
        this.messageFileOptions = value;
    }

    /**
     * Gets the value of the messageFileOutputPath property.
     * 
     * @return
     *     possible object is
     *     {@link java.lang.String }
     *     
     */
    public javafx.beans.property.SimpleStringProperty getMessageFileOutputPath() {
        return messageFileOutputPath;
    }

    /**
     * Sets the value of the messageFileOutputPath property.
     * 
     * @param value
     *     allowed object is
     *     {@link java.lang.String }
     *     
     */
    public void setMessageFileOutputPath(javafx.beans.property.SimpleStringProperty value) {
        this.messageFileOutputPath = value;
    }

    /**
     * Gets the value of the taskInput property.
     * 
     * @return
     *     possible object is
     *     {@link TaskInputModeler }
     *     
     */
    public TaskInput<String, javafx.beans.property.SimpleStringProperty> getTaskInput() {
        return taskInput;
    }

    /**
     * Sets the value of the taskInput property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaskInputModeler }
     *     
     */
    public void setTaskInput(TaskInput<String, javafx.beans.property.SimpleStringProperty> value) {
        this.taskInput = value;
    }

}
