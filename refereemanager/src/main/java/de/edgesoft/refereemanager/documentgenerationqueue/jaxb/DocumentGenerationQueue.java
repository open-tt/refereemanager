
package de.edgesoft.refereemanager.documentgenerationqueue.jaxb;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import de.edgesoft.edgeutils.commons.AbstractModelClass;
import de.edgesoft.edgeutils.commons.Info;
import de.edgesoft.refereemanager.documentgenerationqueue.model.DocumentGenerationJobModel;


/**
 * <p>Java class for DocumentGenerationQueue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DocumentGenerationQueue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{}AbstractModelClass"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="info" type="{}Info"/&gt;
 *         &lt;element name="document_generation_job" type="{}DocumentGenerationJob" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentGenerationQueue", propOrder = {
    "info",
    "documentGenerationJob"
})
public class DocumentGenerationQueue
    extends AbstractModelClass
{

    @XmlElement(required = true)
    protected Info info;
    @XmlElement(name = "document_generation_job", required = true, type = DocumentGenerationJobModel.class)
    protected List<DocumentGenerationJob> documentGenerationJob;

    /**
     * Gets the value of the info property.
     * 
     * @return
     *     possible object is
     *     {@link Info }
     *     
     */
    public Info getInfo() {
        return info;
    }

    /**
     * Sets the value of the info property.
     * 
     * @param value
     *     allowed object is
     *     {@link Info }
     *     
     */
    public void setInfo(Info value) {
        this.info = value;
    }

    /**
     * Gets the value of the documentGenerationJob property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the documentGenerationJob property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocumentGenerationJob().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentGenerationJob }
     * 
     * 
     */
    public List<DocumentGenerationJob> getDocumentGenerationJob() {
        if (documentGenerationJob == null) {
            documentGenerationJob = new ArrayList<DocumentGenerationJob>();
        }
        return this.documentGenerationJob;
    }

}
