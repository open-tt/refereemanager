
package de.edgesoft.refereemanager.documentgenerationqueue.jaxb;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import de.edgesoft.refereemanager.documentgenerationqueue.model.DocumentGenerationJobModel;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.edgesoft.refereemanager.documentgenerationqueue.jaxb package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Documentgenerationqueue_QNAME = new QName("", "documentgenerationqueue");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.edgesoft.refereemanager.documentgenerationqueue.jaxb
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TaskInputModeler }
     * 
     */
    public TaskInputModeler createTaskInputModeler() {
        return new TaskInputModeler();
    }

    /**
     * Create an instance of {@link DocumentGenerationQueue }
     * 
     */
    public DocumentGenerationQueue createDocumentGenerationQueue() {
        return new DocumentGenerationQueue();
    }

    /**
     * Create an instance of {@link DocumentGenerationJob }
     * 
     */
    public DocumentGenerationJob createDocumentGenerationJob() {
        return new DocumentGenerationJobModel();
    }

    /**
     * Create an instance of {@link TaskInputModeler.Entry }
     * 
     */
    public TaskInputModeler.Entry createTaskInputModelerEntry() {
        return new TaskInputModeler.Entry();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentGenerationQueue }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DocumentGenerationQueue }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "documentgenerationqueue")
    public JAXBElement<DocumentGenerationQueue> createDocumentgenerationqueue(DocumentGenerationQueue value) {
        return new JAXBElement<DocumentGenerationQueue>(_Documentgenerationqueue_QNAME, DocumentGenerationQueue.class, null, value);
    }

}
