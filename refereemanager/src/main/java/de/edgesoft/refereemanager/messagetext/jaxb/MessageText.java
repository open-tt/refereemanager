
package de.edgesoft.refereemanager.messagetext.jaxb;

import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.SimpleStringProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import de.edgesoft.edgeutils.commons.AbstractModelClass;
import de.edgesoft.refereemanager.messagetext.model.AttachmentModel;
import de.edgesoft.refereemanager.messagetext.model.ValueMap;


/**
 * <p>Java class for MessageText complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MessageText"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{}AbstractModelClass"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="values" type="{}ValueMapModeler" minOccurs="0"/&gt;
 *         &lt;element name="attachment" type="{}Attachment" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageText", propOrder = {
    "values",
    "attachment"
})
public class MessageText
    extends AbstractModelClass
{

    @XmlElement(type = ValueMapModeler.class)
    protected ValueMap<MessageTextElement, SimpleStringProperty> values;
    @XmlElement(type = AttachmentModel.class)
    protected List<Attachment> attachment;

    /**
     * Gets the value of the values property.
     * 
     * @return
     *     possible object is
     *     {@link ValueMapModeler }
     *     
     */
    public ValueMap<MessageTextElement, SimpleStringProperty> getValues() {
        return values;
    }

    /**
     * Sets the value of the values property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValueMapModeler }
     *     
     */
    public void setValues(ValueMap<MessageTextElement, SimpleStringProperty> value) {
        this.values = value;
    }

    /**
     * Gets the value of the attachment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the attachment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttachment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Attachment }
     * 
     * 
     */
    public List<Attachment> getAttachment() {
        if (attachment == null) {
            attachment = new ArrayList<Attachment>();
        }
        return this.attachment;
    }

}
