
package de.edgesoft.refereemanager.messagetext.jaxb;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MessageTextElement.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <pre>
 * &lt;simpleType name="MessageTextElement"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="BODY"/&gt;
 *     &lt;enumeration value="TITLE"/&gt;
 *     &lt;enumeration value="SUBTITLE"/&gt;
 *     &lt;enumeration value="OPENING"/&gt;
 *     &lt;enumeration value="CLOSING"/&gt;
 *     &lt;enumeration value="SIGNATURE"/&gt;
 *     &lt;enumeration value="DATE"/&gt;
 *     &lt;enumeration value="OPTIONS"/&gt;
 *     &lt;enumeration value="FILENAME"/&gt;
 *     &lt;enumeration value="OUTPUTPATH"/&gt;
 *     &lt;enumeration value="ATTACHMENT"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "MessageTextElement")
@XmlEnum
public enum MessageTextElement {

    BODY,
    TITLE,
    SUBTITLE,
    OPENING,
    CLOSING,
    SIGNATURE,
    DATE,
    OPTIONS,
    FILENAME,
    OUTPUTPATH,
    ATTACHMENT;

    public String value() {
        return name();
    }

    public static MessageTextElement fromValue(String v) {
        return valueOf(v);
    }

}
