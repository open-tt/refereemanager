
package de.edgesoft.refereemanager.messagetext.jaxb;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import de.edgesoft.refereemanager.messagetext.model.AttachmentModel;
import de.edgesoft.refereemanager.messagetext.model.MessageTextModel;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.edgesoft.refereemanager.messagetext.jaxb package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Messagetext_QNAME = new QName("", "messagetext");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.edgesoft.refereemanager.messagetext.jaxb
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ValueMapModeler }
     * 
     */
    public ValueMapModeler createValueMapModeler() {
        return new ValueMapModeler();
    }

    /**
     * Create an instance of {@link MessageText }
     * 
     */
    public MessageText createMessageText() {
        return new MessageTextModel();
    }

    /**
     * Create an instance of {@link Attachment }
     * 
     */
    public Attachment createAttachment() {
        return new AttachmentModel();
    }

    /**
     * Create an instance of {@link ValueMapModeler.Entry }
     * 
     */
    public ValueMapModeler.Entry createValueMapModelerEntry() {
        return new ValueMapModeler.Entry();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MessageText }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link MessageText }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "messagetext")
    public JAXBElement<MessageText> createMessagetext(MessageText value) {
        return new JAXBElement<MessageText>(_Messagetext_QNAME, ((Class) MessageTextModel.class), null, ((MessageTextModel) value));
    }

}
