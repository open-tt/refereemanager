
package de.edgesoft.refereemanager.messagetext.model;

import java.nio.file.Paths;

import org.apache.logging.log4j.util.Strings;

import de.edgesoft.refereemanager.messagetext.jaxb.Attachment;
import de.edgesoft.refereemanager.messagetext.jaxb.ObjectFactory;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;


/**
 * Attachment model.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.17.0
 */
public class AttachmentModel extends Attachment {

	/**
	 * Attachment separator.
	 */
	public static final String ATTACHMENT_SEPARATOR = "::";

	/**
	 * Returns title, filename if no title was given.
	 *
	 * @return title
	 */
	@Override
	public SimpleStringProperty getTitle() {

		if ((title != null) && !Strings.isBlank(title.getValue())) {
			return title;
		}

		if (!Strings.isBlank(getFilename().getValue())) {
			return new SimpleStringProperty(Paths.get(getFilename().getValue()).getFileName().toString());
		}

		return new SimpleStringProperty();

	}

	@Override
	public SimpleStringProperty getFilename() {

		if (filename == null) {
			return new SimpleStringProperty();
		}

		return filename;

	}

	/**
	 * Returns if attachment is landscape.
	 *
	 * @return is landscape
	 */
	@Override
    public SimpleBooleanProperty getLandscape() {
        return landscape == null ? new SimpleBooleanProperty(false) : landscape;
    }

	/**
	 * Returns attachment from string representation.
	 *
	 * @param theString string representation
	 * @return attachment
	 */
	public static Attachment fromFileString(
			final String theString
			) {

		String[] arrAttachmentParts = theString.split(ATTACHMENT_SEPARATOR);


		Attachment attReturn = new ObjectFactory().createAttachment();

		attReturn.setFilename(new SimpleStringProperty(arrAttachmentParts[0].trim()));

		if (arrAttachmentParts.length > 1) {
			attReturn.setTitle(new SimpleStringProperty(arrAttachmentParts[1].trim()));
		}

		if (arrAttachmentParts.length > 2) {
			attReturn.setLandscape(new SimpleBooleanProperty(Boolean.valueOf(arrAttachmentParts[2].trim())));
		}


		return attReturn;

	}

}

/* EOF */
