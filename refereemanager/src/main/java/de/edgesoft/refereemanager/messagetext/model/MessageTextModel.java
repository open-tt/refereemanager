
package de.edgesoft.refereemanager.messagetext.model;

import java.util.ArrayList;
import java.util.List;

import de.edgesoft.refereemanager.messagetext.jaxb.MessageText;
import de.edgesoft.refereemanager.messagetext.jaxb.MessageTextElement;
import de.edgesoft.refereemanager.messagetext.jaxb.ObjectFactory;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;


/**
 * Message text model.
 *
 * Modeling the message text this way contains some pitfalls, strange decisions, but
 * I wanted to have one place to define the contents, which is {@link MessageTextElement}
 *
 * Thus attachments are there twice but the string representation is not used and date
 * is a string only.
 *
 * Maybe there is a better solution for this, for now I have to keep the complexity
 * of the definition low and accept the strangeness.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.17.0
 */
public class MessageTextModel extends MessageText {

	/**
	 * A message variable token.
	 */
	public static final String MESSAGE_VARIABLE_TOKEN = "%s:";

	/**
	 * Creates an instance using objectfactory, initializes hashmap too.
	 *
	 * @return new instance
	 */
	public static MessageText createInstance() {
		MessageText msgReturn = new ObjectFactory().createMessageText();
		msgReturn.setValues(new ValueMap<>());
		return msgReturn;
	}

	/**
	 * Returns message text from string representation.
	 *
	 * @param theString string representation
	 * @return message text
	 */
	public static MessageText fromFileString(
			final String theString
			) {

		MessageText msgReturn = createInstance();

		List<String> lstBody = new ArrayList<>();
		BooleanProperty isBody = new SimpleBooleanProperty(false);

		theString.lines().forEach(line -> {

			if (isBody.getValue()) {

		        lstBody.add(line.stripTrailing());

			} else {

				for (MessageTextElement theTextElement : MessageTextElement.values()) {

					String sVarToken = String.format(MESSAGE_VARIABLE_TOKEN, theTextElement.value().toLowerCase());
					if (line.strip().startsWith(sVarToken)) {

						String theLineContent = line.substring(sVarToken.length()).strip();

						switch (theTextElement) {

							case ATTACHMENT:
								msgReturn.getAttachment().add(AttachmentModel.fromFileString(theLineContent));
								break;

							case BODY:
								// multiple lines without explicit token
								break;

							default:
								msgReturn.getValues().put(theTextElement, new SimpleStringProperty(theLineContent));
								break;

						}
					}

				}

			}

			if (line.isEmpty()) {
				isBody.setValue(true);
			}

		});

		msgReturn.getValues().put(MessageTextElement.BODY, new SimpleStringProperty(String.join(System.lineSeparator(), lstBody)));

		return msgReturn;

	}

	/**
	 * Returns value for message text element.
	 *
	 * @param theMessageTextElement element key
	 * @return value
	 */
	public String getValueFor(
			final MessageTextElement theMessageTextElement
			) {

		if (!getValues().containsKey(theMessageTextElement)) {
			return null;
		}

		return getValues().get(theMessageTextElement).getValue();

	}

	/**
	 * Returns value for message text element (string representation).
	 *
	 * This is a convenience method for access by freemarker templates, as I
	 * don't know how to call with a {@link MessageTextElement} from freemarker.
	 *
	 * TODO check if freemarker can call {@link #getValueFor(MessageTextElement)} directly
	 *
	 * @param theMessageTextElement element key as string
	 * @return value
	 */
	public String getValueFor(
			final String theMessageTextElement
			) {

		return getValueFor(MessageTextElement.fromValue(theMessageTextElement.toUpperCase()));

	}

}

/* EOF */
