
package de.edgesoft.refereemanager.messagetext.model;

import java.util.Map;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import de.edgesoft.refereemanager.messagetext.jaxb.MessageTextElement;
import de.edgesoft.refereemanager.messagetext.jaxb.ValueMapModeler;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


/**
 * Value map adapter.
 *
 * Inspired by http://todayguesswhat.blogspot.com/2012/09/jaxb-xsd-to-java-maphashmap-example.html
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.17.0
 */
public class ValueMapAdapter extends XmlAdapter<ValueMapModeler, ValueMap<MessageTextElement, StringProperty>> {

	@Override
	public ValueMap<MessageTextElement, StringProperty> unmarshal(ValueMapModeler theModeler) throws Exception {

		ValueMap<MessageTextElement, StringProperty> mapReturn = new ValueMap<>();

		for (ValueMapModeler.Entry e : theModeler.getEntry()) {
			mapReturn.put(e.getKey(), e.getValue());
		}

		return mapReturn;

	}

	@Override
	public ValueMapModeler marshal(ValueMap<MessageTextElement, StringProperty> map) throws Exception {

		ValueMapModeler modeler = new ValueMapModeler();

		for (Map.Entry<MessageTextElement, StringProperty> entry : map.entrySet()) {

			ValueMapModeler.Entry e = new ValueMapModeler.Entry();
			e.setKey(entry.getKey());
			e.setValue(new SimpleStringProperty(entry.getValue().get()));
			modeler.getEntry().add(e);

		}

		return modeler;

	}

}

/* EOF */
