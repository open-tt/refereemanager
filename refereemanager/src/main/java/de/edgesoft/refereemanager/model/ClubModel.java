package de.edgesoft.refereemanager.model;

import java.util.function.Predicate;

import de.edgesoft.edgeutils.files.FileUtils;
import de.edgesoft.refereemanager.jaxb.Club;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Club model, additional methods for jaxb model class.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.5.0
 */
public class ClubModel extends Club {

	/**
	 * Filter predicate for local clubs.
	 */
	public static Predicate<Club> LOCAL = club -> club.getIsLocal().getValue();

	/**
	 * Filter predicate for non-local clubs.
	 */
	public static Predicate<Club> NON_LOCAL = LOCAL.negate();

	/**
	 * Returns if club is local club.
	 *
	 * @return is club local club
	 *
	 * @since 0.9.0
	 */
	@Override
	public SimpleBooleanProperty getIsLocal() {
		return (super.getIsLocal() == null) ? new SimpleBooleanProperty(Boolean.FALSE) : super.getIsLocal();
	}

	/**
	 * Filename.
	 *
	 * @return filename
	 *
	 * @since 0.12.0
	 */
	@Override
	public SimpleStringProperty getFilename() {

		if ((super.getFilename() == null) || (super.getFilename().getValueSafe().isEmpty())) {
			return new SimpleStringProperty(FileUtils.cleanFilename(getDisplayText().getValueSafe(), false));
		}

		return new SimpleStringProperty(FileUtils.cleanFilename(super.getFilename().getValueSafe(), false));
	}

	/**
	 * Returns display text.
	 *
	 * @return display text
	 */
	@Override
	public StringProperty getDisplayText() {
		return getDisplayTitleShort();
	}

}

/* EOF */
