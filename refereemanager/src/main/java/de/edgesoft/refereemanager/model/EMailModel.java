package de.edgesoft.refereemanager.model;

import de.edgesoft.refereemanager.jaxb.EMail;
import javafx.beans.property.SimpleStringProperty;

/**
 * EMail model, additional methods for jaxb model class.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.6.0
 */
public class EMailModel extends EMail {

	/**
	 * Display text.
	 *
	 * @return display text
	 */
	@Override
	public SimpleStringProperty getDisplayText() {

		if (isPrivateOnly && !isPrivate()) {
			return null;
		}

		StringBuilder sbReturn = new StringBuilder();

		sbReturn.append(getEMail().getValue());

		if (!isPrivate()) {
			sbReturn.append(" (");
			sbReturn.append(getContactType().getShorttitle().getValue());
			sbReturn.append(")");
		}

		return new SimpleStringProperty(sbReturn.toString());

	}

}

/* EOF */
