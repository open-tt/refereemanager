package de.edgesoft.refereemanager.model;

import java.text.MessageFormat;
import java.util.Comparator;

import de.edgesoft.refereemanager.jaxb.RefereeQuantity;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Referee quantity model, additional methods for jaxb model class.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.17.0
 */
public class RefereeQuantityModel extends RefereeQuantity {

	/**
	 * Comparator rank.
	 */
	public static final Comparator<RefereeQuantity> RANK = Comparator.comparingInt(quantity -> quantity.getRefereeAssignmentType().getRank().get());

	/**
	 * Default comparator.
	 */
	public static final Comparator<RefereeQuantity> DEFAULT_COMPARATOR = RANK;

	@Override
	public StringProperty getDisplayText() {
		return new SimpleStringProperty(MessageFormat.format("{0}: {1, number}",
				getRefereeAssignmentType().getDisplayTitleShort().getValueSafe(),
				getQuantity().intValue()));
	}

}

/* EOF */
