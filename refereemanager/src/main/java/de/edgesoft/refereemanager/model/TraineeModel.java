package de.edgesoft.refereemanager.model;

import java.util.function.Predicate;

import de.edgesoft.refereemanager.jaxb.Trainee;
import de.edgesoft.refereemanager.utils.ExamResult;
import javafx.beans.property.SimpleBooleanProperty;

/**
 * Trainee model, additional methods for jaxb model class.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.12.0
 */
public class TraineeModel extends Trainee {

	/**
	 * Filter predicate for trainees that passed the exam.
	 */
	public static Predicate<Trainee> PASSED = trainee -> trainee.getPassed().getValue();

	/**
	 * Returns filter predicate for given exam result.
	 *
	 * @param theExamResult exam result
	 * @return predicate
	 */
	public static Predicate<Trainee> getExamResultPredicate(ExamResult theExamResult) {
		return trainee -> ((TraineeModel) trainee).getExamResult() == theExamResult;
	}

	/**
	 * Has referee passed exam.
	 *
	 * @return has referee passed exam?
	 */
	@Override
	public SimpleBooleanProperty getPassed() {
		if (super.getPassed() == null) {
			return new SimpleBooleanProperty(false);
		}
		return super.getPassed();
	}

	/**
	 * What is the exam result?
	 *
	 * @return has referee passed exam?
	 */
	public ExamResult getExamResult() {

		if (getWithdrawn().getValue()) {
			return ExamResult.WITHDRAWN;
		}

		if (getDidNotStart().getValue()) {
			return ExamResult.NO_START;
		}

		if (getExamDate() == null) {
			return ExamResult.NOT_YET;
		}

		return (getPassed().get()) ? ExamResult.PASSED : ExamResult.NOT_PASSED;

	}

}

/* EOF */
