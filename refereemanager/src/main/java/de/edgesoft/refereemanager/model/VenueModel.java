package de.edgesoft.refereemanager.model;

import de.edgesoft.edgeutils.files.FileUtils;
import de.edgesoft.refereemanager.jaxb.Address;
import de.edgesoft.refereemanager.jaxb.Venue;
import javafx.beans.property.SimpleStringProperty;

/**
 * Venue model, additional methods for jaxb model class.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.15.0
 */
public class VenueModel extends Venue {

	/**
	 * Primary address.
	 *
	 * @return primary address
	 *
	 * @since 0.17.0
	 */
	public Address getPrimaryAddress() {

		if (getAddress().size() == 1) {
			return getAddress().get(0);
		}

		return getAddress()
				.stream()
				.filter(ContactModel.ISPRIMARY)
				.findFirst()
				.orElse(null);
	}

	/**
	 * Primary geolocation.
	 *
	 * @return primary geolocation
	 */
	public SimpleStringProperty getPrimaryGeolocation() {

		if (getPrimaryAddress() == null) {
			return null;
		}

		return ((AddressModel) getPrimaryAddress()).getGeolocation();

	}

	/**
	 * Filename.
	 *
	 * @return filename
	 *
	 * @since 0.17.0
	 */
	public SimpleStringProperty getFilename() {
		return new SimpleStringProperty(FileUtils.cleanFilename(getDisplayTitleShort().getValueSafe(), true));
	}

}

/* EOF */
