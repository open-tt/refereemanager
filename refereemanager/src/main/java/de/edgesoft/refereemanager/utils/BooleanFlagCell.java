package de.edgesoft.refereemanager.utils;

import de.edgesoft.edgeutils.files.Prefs;
import de.edgesoft.edgeutils.files.Resources;
import de.edgesoft.edgeutils.i18n.I18N;
import javafx.scene.control.TableCell;
import javafx.scene.image.ImageView;

/**
 * Table cell for boolean flags.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.17.0
 */
public class BooleanFlagCell<T> extends TableCell<T, Boolean> {

	/**
	 * Image view for icon.
	 */
	private ImageView image = null;

	/**
	 * Constructor, creating image view.
	 */
	public BooleanFlagCell() {

		image = new ImageView();
        setGraphic(image);

	}

	@Override
    protected void updateItem(Boolean item, boolean empty) {
        super.updateItem(item, empty);

        if (empty || (item == null)) {
            image.setImage(null);
        } else {
        	image.setImage(Resources.loadImage(
					I18N.getText(String.format("refman.emblem.%s.icon", item ? "success" : "error")).get(),
					Prefs.SIZE_FLAG));
        }
    }

}

/* EOF */
