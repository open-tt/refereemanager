package de.edgesoft.refereemanager.utils;


import de.edgesoft.edgeutils.files.Prefs;
import de.edgesoft.edgeutils.files.Resources;
import de.edgesoft.edgeutils.javafx.ButtonUtils;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.image.ImageView;

/**
 * Utility methods for {@link Resources}.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.17.0
 */
public class ResourceUtils {

	/**
	 * Sets graphic on button(s).
	 *
	 * @param theImagePath image path
	 * @param theButtons buttons
	 */
	public static final void setGraphic(
			final String theImagePath,
			Button... theButtons
			) {

		for (Button theButton : theButtons) {
			theButton.setGraphic(new ImageView(Resources.loadImage(theImagePath, Prefs.SIZE_BUTTON_SMALL)));
		}

	}

	/**
	 * Sets graphic on menu item(s).
	 *
	 * @param theImagePath image path
	 * @param theMenuItem menu items
	 */
	public static final void setGraphic(
			final String theImagePath,
			MenuItem... theMenuItems
			) {

		for (MenuItem theMenuItem : theMenuItems) {
			setGraphic(theImagePath, theMenuItem, null);
		}

	}

	/**
	 * Sets graphic on menu item and button.
	 *
	 * @param theImagePath image path
	 * @param theMenuItem menu item
	 * @param theButton button
	 */
	public static final void setGraphic(
			final String theImagePath,
			MenuItem theMenuItem,
			Button theButton
			) {

		theMenuItem.setGraphic(new ImageView(Resources.loadImage(theImagePath)));

		if (theButton != null) {
			ButtonUtils.adaptButton(theButton, theMenuItem);
		}

	}

}

/* EOF */
