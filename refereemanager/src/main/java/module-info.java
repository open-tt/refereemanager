/**
 * Module info file.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of Open-TT: Referee Manager.</p>
 *
 * <p>Open-TT: Referee Manager is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * <p>Open-TT: Referee Manager is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.16.0
 */
module de.edgesoft.refereemanager {

	// module exports
	exports de.edgesoft.refereemanager;
	exports de.edgesoft.refereemanager.controller to javafx.fxml;
//	exports de.edgesoft.refereemanager.controller.crud to javafx.fxml;
//	exports de.edgesoft.refereemanager.controller.datatables to javafx.fxml;
//	exports de.edgesoft.refereemanager.controller.details to javafx.fxml;
//	exports de.edgesoft.refereemanager.controller.editdialogs to javafx.fxml;
//	exports de.edgesoft.refereemanager.controller.help to javafx.fxml;
//	exports de.edgesoft.refereemanager.controller.inputforms to javafx.fxml;
	exports de.edgesoft.refereemanager.controller.overview to javafx.fxml;

	exports de.edgesoft.refereemanager.jaxb to de.edgesoft.refereemanager;
	exports de.edgesoft.refereemanager.model to de.edgesoft.refereemanager;
	exports de.edgesoft.refereemanager.documentgenerationqueue.jaxb to de.edgesoft.refereemanager;
	exports de.edgesoft.refereemanager.documentgenerationqueue.model to de.edgesoft.refereemanager;
	exports de.edgesoft.refereemanager.messagetext.jaxb to de.edgesoft.refereemanager, freemarker;
	exports de.edgesoft.refereemanager.messagetext.model to de.edgesoft.refereemanager, freemarker;
	exports de.edgesoft.refereemanager.utils to de.edgesoft.refereemanager, freemarker;

	// opens to all
	opens de.edgesoft.refereemanager.resources.i18n;

	// opens to edgeutils and other helper classes in order to load views and resources
	opens de.edgesoft.refereemanager.view to de.edgesoft.edgeutils;
	opens de.edgesoft.refereemanager.view.crud to de.edgesoft.edgeutils;
	opens de.edgesoft.refereemanager.view.datatables to de.edgesoft.edgeutils;
	opens de.edgesoft.refereemanager.view.details to de.edgesoft.edgeutils;
	opens de.edgesoft.refereemanager.view.editdialogs to de.edgesoft.edgeutils;
	opens de.edgesoft.refereemanager.view.help to de.edgesoft.edgeutils;
	opens de.edgesoft.refereemanager.view.inputforms to de.edgesoft.edgeutils;
	opens de.edgesoft.refereemanager.view.overview to de.edgesoft.edgeutils;
	opens de.edgesoft.refereemanager.resources to de.edgesoft.edgeutils;
	opens de.edgesoft.refereemanager.resources.css to de.edgesoft.edgeutils;
	opens de.edgesoft.refereemanager.resources.icons.actions to de.edgesoft.edgeutils;
	opens de.edgesoft.refereemanager.resources.icons.emblems to de.edgesoft.edgeutils;
	opens de.edgesoft.refereemanager.resources.icons.own to de.edgesoft.edgeutils;
	opens de.edgesoft.refereemanager.resources.icons.status to de.edgesoft.edgeutils;
	opens de.edgesoft.refereemanager.resources.images to de.edgesoft.edgeutils;

	// opens to fxml for controller loading from fxml file
	opens de.edgesoft.refereemanager.controller to javafx.fxml;
	opens de.edgesoft.refereemanager.controller.crud to javafx.fxml;
	opens de.edgesoft.refereemanager.controller.datatables to javafx.fxml;
	opens de.edgesoft.refereemanager.controller.details to javafx.fxml;
	opens de.edgesoft.refereemanager.controller.editdialogs to javafx.fxml;
	opens de.edgesoft.refereemanager.controller.help to javafx.fxml;
	opens de.edgesoft.refereemanager.controller.inputforms to javafx.fxml;
	opens de.edgesoft.refereemanager.controller.overview to javafx.fxml;

	// opens to xml.bind and freemarker for using jaxb classes
	opens de.edgesoft.refereemanager.jaxb to java.xml.bind, freemarker;

	// opens to all, because it is needed by a unnamed module
	// TODO find out, why this is needed
	opens de.edgesoft.refereemanager.model;


	// simple requirements
	requires java.xml.bind;
	requires javafx.controls;
	requires javafx.fxml;
	requires javafx.graphics;
	requires javafx.web;
	requires org.testfx;

	// transitive requirements
	requires transitive de.edgesoft.edgeutils;
	requires transitive org.apache.logging.log4j.core;
	requires transitive javax.mail.api;

	// transitive requirements with unstable names - no module info in the package
	requires transitive freemarker;

}

/* EOF */
