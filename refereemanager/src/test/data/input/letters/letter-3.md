title: Testletter 3
opening: Hallo ${current.firstName.value},
closing: Gruß,
signature: Ekkart.
attachment: /home/ekleinod/working/git/open-tt/refereemanager/refereemanager/src/test/data/one_page_landscape.pdf :: One fine page :: true
<#if current.status.active.value >
attachment: /home/ekleinod/working/git/open-tt/refereemanager/refereemanager/src/test/data/two_pages_portrait.pdf
</#if>
filename:	${current.fileName.value}.md
outputpath: Briefe/temp

einleitender Text, sichtbar für alle.

<#if current.status.active.value >

# Nur sichtbar für aktive SR

Informationen für aktive SR

</#if>

# Sichtbar wieder für alle

Weitere Informationen, die für alle wichtig sind.

Abschließende Bemerkungen vor der Schlussformel.
